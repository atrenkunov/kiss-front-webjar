module.exports = {
    parser: '@babel/eslint-parser',
    parserOptions: {
        ecmaVersion: 2018,
        sourceType: 'module'
    },
    plugins: [
        'react-hooks'
    ],
    extends: [
        './config/eslint/a11y',
        './config/eslint/babel',
        './config/eslint/es6',
        './config/eslint/import',
        './config/eslint/react',
        './config/eslint/stylistic',
        'plugin:react/recommended',
    ],
    rules: {
        'react-hooks/rules-of-hooks': 'error',
        'react-hooks/exhaustive-deps': 'warn',
        'react/prop-types': 'off',
    },
    settings: {
        react: {
            pragma: 'React',
            version: 'detect'
        }
    }
}
