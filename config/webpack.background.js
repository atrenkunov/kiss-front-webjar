const path = require('path')

const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const BrowserExtensionPlugin = require("extension-build-webpack-plugin")
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

const config = {
    entry: './src/background.js',
    output: {
        path: path.resolve(__dirname, '..', 'dist_ext'),
        filename: 'background.js'
    },
    module: {
        rules: [
        {
            test: /\.(js|jsx)$/,
            use: 'babel-loader'
        }
        ]
    },
    plugins: [
        new BrowserExtensionPlugin({devMode: false, name: "extension.zip", directory: "src", updateType: "minor"}),
    ]
}

module.exports = config
