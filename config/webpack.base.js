const path = require('path')
const fs = require('fs')

const HtmlWebpackPlugin= require('html-webpack-plugin')

function getPackageDir (filepath) {
    let currDir = path.dirname(require.resolve(filepath))
    while (true) {
        if (fs.existsSync(path.join(currDir, 'package.json'))) {
            return currDir
        }
        const { dir, root } = path.parse(currDir)
        if (dir === root) {
            throw new Error(
                `Could not find package.json in the parent directories starting from ${filepath}.`
            )
        }
        currDir = dir
    }
}

const config = {
    entry: './src/index.jsx',
    resolve: {
        extensions: ['.jsx', '.js'],
        alias: {
            '@styles': path.resolve(__dirname, '..', 'src/assets/styles'),
            '@emotion/core': getPackageDir('@emotion/react'),
            '@emotion/styled': getPackageDir('@emotion/styled'),
            'emotion-theming': getPackageDir('@emotion/react'),
        }
    },
    module: {
        rules: [
            {
                test: /\.(js)x?$/,
                exclude: [/node_modules/, /node_modules[\\/](?!((@sbol).*)[\\/]).*/],
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env',
                            '@babel/preset-react',
                        ],
                    },
                },
            },
            {
                test: /\.m?js/,
                resolve: {
                    fullySpecified: false
                }
            },
            {
                test: /\.svg$/,
                // import icon from './icon.svg' // icon === '<svg><path /></svg>'
                loader: 'svg-inline-loader',
                options: {
                    // Удалять пустые теги
                    removeTags: true,
                    // Всегда удалять эти теги
                    removingTags: ['title', 'desc'],
                    // Удалять атрибуты с тэга svg
                    removeSVGTagAttrs: false
                }
            },
            {
                test: /\.(png|jpg|jpeg|gif)$/i,
                type: 'asset/resource'
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)/i,
                type: 'asset/resource',
            }
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(__dirname, '..', 'public', 'index.html'),
        }),
    ]
}

module.exports = config
