const path = require('path')

const openBrowser = require('react-dev-utils/openBrowser')
const webpack = require('webpack')
const { merge } = require('webpack-merge')

const base = require('./webpack.base')

const config = merge(base, {
    mode: 'development',
    output: {
        publicPath: '/',
    },
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: ['style-loader', {
                    loader: 'css-loader',
                    options: {
                        modules: true,
                    },

                }, 'postcss-loader'],
            },
        ]
    },
    devtool: 'inline-source-map',
    devServer: {
        after: () => {
            openBrowser('http://localhost:3000')
        },
        contentBase: path.join(__dirname, '..', 'public'),
        watchContentBase: true,
        hot: true,
        compress: true,
        port: 3000,
        overlay: true,
        stats: 'errors-only',
        historyApiFallback: true,
        proxy: {
            '/api': {
                target: 'http://130.193.55.12:8081/api',
                secure: false,
                pathRewrite: {
                    '^/api': ''
                }
            },
            '/view/': {
                target: 'http://130.193.55.12:8081/view',
                secure: false,
                pathRewrite: {
                    '^/view/': ''
                }
            },

        },
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
    ],
})

module.exports = config
