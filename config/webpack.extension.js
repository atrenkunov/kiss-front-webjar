const path = require('path')

const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const BrowserExtensionPlugin = require("extension-build-webpack-plugin")
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const { merge } = require('webpack-merge')

const base = require('./webpack.base')

const config = merge(base, module.exports = {
    output: {
        path: path.resolve(__dirname, '..', 'dist_ext'),
        filename: '[name].[contenthash].js'
    },
    module: {
        rules: [
        {
            test: /\.(js|jsx)$/,
            use: 'babel-loader'
        },
        {
            test: /\.css$/i,
            use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader'],
        }
        ]
    },
    plugins: [
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: path.join(__dirname, '..', 'public', 'assets'),
                    to: 'assets/[hash].[ext]',
                    noErrorOnMissing: true,
                },
                {
                    from: path.join(__dirname, '..', 'public', 'manifest.json'),
                    to: 'manifest.json',
                },
                {
                    from: path.join(__dirname, '..', 'public', 'favicon-16x16.png'),
                    to: 'favicon-16x16.png',
                },
                {
                    from: path.join(__dirname, '..', 'public', 'favicon-32x32.png'),
                    to: 'favicon-32x32.png',
                }
            ]
        }),
        new MiniCssExtractPlugin({}),
        new BrowserExtensionPlugin({devMode: false, name: "my-first-webpack.zip", directory: "src", updateType: "minor"}),
    ]
})

module.exports = config
