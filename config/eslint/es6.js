module.exports = {
    env: { es6: true },
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
        ecmaFeatures: {
            generators: false,
            jsx: false,
            objectLiteralDuplicateProperties: false
        }
    },
    rules: {
        'arrow-body-style': ['warn', 'as-needed', {
            requireReturnForObjectLiteral: false
        }],
        'arrow-parens': ['warn', 'always'],
        'arrow-spacing': ['warn', { before: true, after: true }],
        'generator-star-spacing': ['error', { before: false, after: true }],
        'no-confusing-arrow': ['off', {
            allowParens: true
        }],
        'no-const-assign': 'error',
        'no-restricted-imports': 'off',
        'no-useless-computed-key': 'warn',
        'no-useless-rename': ['warn', {
            ignoreDestructuring: false,
            ignoreImport: false,
            ignoreExport: false,
        }],
        'no-var': 'warn',
        'object-shorthand': ['warn', 'always', {
            ignoreConstructors: false,
            avoidQuotes: true,
        }],
        'prefer-arrow-callback': ['warn', {
            allowNamedFunctions: false,
            allowUnboundThis: true,
        }],
        'prefer-const': ['warn', {
            destructuring: 'any',
            ignoreReadBeforeAssign: true
        }],
        'prefer-destructuring': ['warn', {
            array: false,
            object: true
        }, {
            enforceForRenamedProperties: false
        }],
        'prefer-numeric-literals': 'error',
        'prefer-rest-params': 'warn',
        'prefer-spread': 'warn',
        'prefer-template': 'warn',
        'rest-spread-spacing': ['warn', 'never'],
        'sort-imports': ['off', {
            ignoreCase: false,
            ignoreMemberSort: false,
            memberSyntaxSortOrder: ['none', 'all', 'multiple', 'single'],
        }],
        'template-curly-spacing': ['warn', 'never'],
    }
}
