module.exports = {
    env: { es6: true },
    parserOptions: {
        ecmaVersion: 2017,
        sourceType: 'module'
    },
    plugins: ['import'],
    rules: {
        'import/named': 'off',
        'import/default': 'off',
        'import/export': 'error',
        'import/no-named-as-default': 'warn',
        'import/first': ['warn', 'absolute-first'],
        'import/imports-first': 'off',
        'import/no-duplicates': 'warn',
        'import/order': ['warn', {
            groups: ['builtin', 'external', 'internal', 'parent', 'sibling', 'index'],
            'newlines-between': 'always'
        }],
        'import/newline-after-import': 'warn',
        'import/prefer-default-export': 'off',
        'import/max-dependencies': ['off', { max: 10 }],
        'import/no-absolute-path': 'error',
        'import/no-dynamic-require': 'error',
        'import/no-unassigned-import': 'off',
        'import/no-named-default': 'warn'
    }
}