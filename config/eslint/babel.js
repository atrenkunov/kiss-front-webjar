module.exports = {
    plugins: ['babel'],
    rules: {
        'babel/camelcase': ['warn', { properties: 'always' }],
        'babel/quotes': ['warn', 'single', { avoidEscape: true, allowTemplateLiterals: false }],
        'babel/no-unused-expressions': ['error', {
            allowShortCircuit: false,
            allowTernary: false
        }],
        'babel/object-curly-spacing': ['off', 'always'],
    }
}