module.exports = {
    plugins: ['react'],
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
    },
    rules: {
        'jsx-quotes': ['warn', 'prefer-double'],
        'react/display-name': ['off', { ignoreTranspilerName: false }],
        'react/forbid-prop-types': ['warn', { forbid: ['any'] }],
        'react/jsx-boolean-value': ['warn', 'never'],
        'react/jsx-closing-bracket-location': ['warn', 'line-aligned'],
        'react/jsx-curly-spacing': ['warn', 'never', { allowMultiline: true }],
        'react/jsx-curly-brace-presence': ['warn', { 'props': 'never', 'children': 'ignore' }],
        'react/jsx-handler-names': ['warn', {
            eventHandlerPrefix: 'handle',
            eventHandlerPropPrefix: 'on',
        }],
        'react/jsx-key': 'warn',
        'react/jsx-max-props-per-line': ['warn', { maximum: 1, when: 'multiline' }],
        'react/jsx-no-bind': ['error', {
            ignoreRefs: true,
            allowArrowFunctions: false,
            allowBind: false,
        }],
        'react/jsx-no-duplicate-props': ['error', { ignoreCase: true }],
        'react/jsx-no-literals': 'warn',
        'react/jsx-no-undef': 'error',
        'react/jsx-pascal-case': ['warn', {
            allowAllCaps: true,
            ignore: [],
        }],
        'react/sort-prop-types': ['off', {
            ignoreCase: true,
            callbacksLast: true,
            requiredFirst: false,
        }],
        'react/jsx-sort-props': ['off', {
            ignoreCase: true,
            callbacksLast: false,
            shorthandFirst: false,
            shorthandLast: false,
            noSortAlphabetically: false,
        }],
        'react/jsx-uses-react': ['error'],
        'react/jsx-uses-vars': 'warn',
        'react/no-danger': 'warn',
        'react/no-deprecated': 'error',
        'react/no-multi-comp': ['warn', { ignoreStateless: true }],
        'react/no-string-refs': 'error',
        'react/no-unknown-property': 'warn',
        'react/prefer-es6-class': ['warn', 'always'],
        'react/prefer-stateless-function': ['warn', { ignorePureComponents: true }],
        'react/prop-types': ['warn', {
            ignore: [],
            customValidators: [],
            skipUndeclared: false
        }],
        'react/react-in-jsx-scope': 'error',
        'react/self-closing-comp': 'warn',
        'react/jsx-tag-spacing': ['warn', {
            closingSlash: 'never',
            beforeSelfClosing: 'always',
            afterOpening: 'never'
        }],
        'react/jsx-wrap-multilines': ['warn', {
            declaration: true,
            assignment: true,
            return: true
        }],
        'react/jsx-first-prop-new-line': ['warn', 'multiline'],
        'react/jsx-equals-spacing': ['warn', 'never'],
        'react/jsx-indent': ['warn', 4],
        'react/jsx-indent-props': ['warn', 4],
        'react/no-array-index-key': 'warn',
        'react/require-default-props': 'warn',
    }
}