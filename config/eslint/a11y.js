module.exports = {
    plugins: ['jsx-a11y'],
    rules: {
        'jsx-a11y/anchor-has-content': 'error',
        'jsx-a11y/heading-has-content': 'warn',

        'jsx-a11y/alt-text': ['warn', {
            elements: ['img', 'object', 'area', 'input[type="image"]']
        }],
        'jsx-a11y/img-redundant-alt': ['warn', {
            elements: ['img'],
            // Which words are redundant as substring of alt
            words: ['Иконка', 'Картинка']
        }],

        'jsx-a11y/aria-props': 'warn',
        'jsx-a11y/aria-proptypes': 'warn',
        'jsx-a11y/aria-unsupported-elements': 'warn',
        'jsx-a11y/role-has-required-aria-props': 'warn',
        'jsx-a11y/role-supports-aria-props': 'warn',

        'jsx-a11y/click-events-have-key-events': 'warn',
        'jsx-a11y/mouse-events-have-key-events': 'warn',
        'jsx-a11y/interactive-supports-focus': 'error',
        'jsx-a11y/no-access-key': 'warn',
        'jsx-a11y/no-onchange': 'warn',
        'jsx-a11y/no-redundant-roles': 'warn',
        'jsx-a11y/no-static-element-interactions': [
            'warn',
            {
                handlers: [
                    'onClick',
                    'onMouseDown',
                    'onMouseUp',
                    'onKeyPress',
                    'onKeyDown',
                    'onKeyUp'
                ]
            }
        ],
    }
}