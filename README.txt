Фронтовое приложение для kiss-front-app.

ПО для сборки: gradle и mvn
Как собрать: gradle buildFront

Исходные файлы: в папке dist
Выходной артефакт: kiss-front-webjar.jar в папке build/libs

Для внедрения в SpringBoot необходимо указать в зависимостях

<dependency>
    <groupId>kiss</groupId>
    <artifactId>kiss-front-webjar</artifactId>
    <version>1</version>
</dependency>