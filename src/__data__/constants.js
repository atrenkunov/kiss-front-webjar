export const LIGHT_THEME = 'LIGHT_THEME'
export const DARK_THEME = 'DARK_THEME'

export const APP_ROUTES = {
    LOGIN: '/login',
    PROFILE: '/profile',

    MAIN_PAGE: '/',
    REPOSITORY: '/repository/:repoId',
    CREATE_NEW_REPOSITORY: '/repository/create',

    BUILDS: '/builds',
    BUILD: '/build/:buildId',
    TASKS: '/tasks',
    TASKS_NEW: '/tasks/create',
    TASK_NEW_BRANCH: '/tasks/:taskId/create-branch',
    PLANNER: '/planner',
    INTEGRATIONS: '/integrations',
    STANDS: '/stands',
    CREATE_NEW_STAND: '/stand/create/:segment',
    VIEWER: '/viewer/:viewId'
}

export const API_RESPONSE_STATUSES = {
    OK: 200,
    SERVER: 500
}

export const STANDS_TYPE = {
    DEV: 'DEV',
    IFT: 'IFT',
    ST: 'ST',
    PSI: 'PSI',
    PROM: 'PROM'
}

export const BRANCH_TYPES = {
    FEATURE: 'feature',
    HOTFIX: 'hotfix',
    FIX: 'fix',
    RELEASE: 'release',
}

export const TASK_TYPES = {
    STORY: 'История',
    TASK: 'Задача',
    BUG: 'Баг'
}

export const EXECUTOR_STAGES_TYPES = {
    INSTALLATION: 'install',
    BUILD: 'build'
}

export const HTML_ENTITIES = {
    EM_DASH: '&mdash;'
}

export const STAGE_DESCRIPTIONS = {
    SKIPPED: 'Пропущено',
    COMPLETED: 'Завершено',
    WAITING: 'Ожидание',
    RUNNING: 'Выполняется',
}

export const TASK_NEW_BRANCH_LINK = (id) => `/tasks/${encodeURIComponent(id)}/create-branch`
export const REPOSITORY_LINK = (id) => `/repository/${encodeURIComponent(id)}`
export const BUILD_LINK = (id) => `/build/${encodeURIComponent(id)}`
export const VIEWER_LINK = (id) => `/viewer/${encodeURIComponent(id)}`
export const CREATE_STAND_LINK = (segment) => `/stand/create/${encodeURIComponent(segment)}`