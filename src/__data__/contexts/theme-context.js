import { createContext } from 'react'

import { LIGHT_THEME } from '../constants'

const defaultContext = {
    theme: LIGHT_THEME,
    setTheme: (theme) => ({})
}
export const ThemeContext = createContext(defaultContext)
ThemeContext.displayName = 'ThemeContext'
