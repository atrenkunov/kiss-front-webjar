import axios from 'axios'
import { config } from '../constants/config'

export const axiosInstance = axios.create({
    timeout: 10000,
    baseURL: config.isExtension
        ? config.baseURL
        : window.location.origin,
    responseType: 'json',
    withCredentials: true,
    headers: { 'Content-Type': 'application/json;charset=UTF-8' }
})
