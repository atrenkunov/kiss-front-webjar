import { axiosInstance as axios } from './axios-config'

export const USER_API = {
    login: (data) => axios.post('/api/auth/login', data),
    info: () => axios.get('/api/auth/info'),
}

export const REPOS_API = {
    getRepo: (id) => axios.get(`/api/repo/${encodeURIComponent(id)}`),
    getAll: (teamId) => axios.get(`/api/repo/all/byTeam?teamId=${encodeURIComponent(teamId)}`),

    createBranch: (repoId, data) => axios.post(`/api/repo/${encodeURIComponent(repoId)}/branch`, data),
    deleteBranch: (repoId, branchId) => axios.post(`/api/repo/${encodeURIComponent(repoId)}/branch/${branchId}`)
}

export const JIRA_API = {
    getTasks: () => axios.get('/api/jira/task/all'),
    getTeamTasks: (teamId) => axios.get(`/api/jira/task/byTeamIdInWork?teamId=${teamId}&selected=true`),
    selectTask: (data) => axios.post('/api/jira/task/select', data),
    createTask: (data) => axios.post('/api/jira/task', data),

    requestPage: (viewId) => axios.get(`/view/confluence/sigma/${viewId}`)
}

export const NEXUS_API = {
    getArtifacts: (teamId) => axios.get(`/api/nexus/artifact/all/byTeam?teamId=${teamId}`),
    getStands: (standType, teamId) => axios.get(`/api/machine/${standType}/byTeam?teamId=${teamId}`),
    install: (data) => axios.post(`/api/machine/${data.machineId}/install`, data),
    createStand: (data) => axios.post('/api/machine', data)
}

export const EXECUTOR_API = {
    build: (data) => axios.post('/api/executor/build', data),
    getStages: (type) => axios.get(`/api/executor/${type}/stage/all`),

    installStatus: (installId) => axios.get(`/api/executor/install/${installId}/status`),
    getActiveBuildsByTeamId: (teamId) => axios.get(`/api/executor/build/all/byTeam?teamId=${encodeURIComponent(teamId)}`),
    getStatus: (buildId) => axios.get(`/api/executor/build/${encodeURIComponent(buildId)}/status`),
    getArtifactNumber: (buildId) => axios.get(`api/executor/build/${encodeURIComponent(buildId)}/result`),
}

export const INTEGRATION_API = {
    getIntegrations: (teamId) => axios.get(`/api/integration/all/byTeam=${encodeURIComponent(teamId)}`),
    addIntegration: (data) => axios.post('/api/integration', data),
    getIntegrationLog: (integrationId) => axios.get(`/api/integration/${encodeURIComponent(integrationId)}/log`),
    getIntegrationTypes: () => axios.get('/api/integration/integrationTypes'),
    deleteIntegration: (integrationId) => axios.delete(`/api/integration/${encodeURIComponent(integrationId)}`)
}

export const TEAM_API = {
    getAllTeams: () => axios.get('/api/team/all'),
    getTeam: (teamId) => axios.get(`/api/team/${teamId}`)
}
