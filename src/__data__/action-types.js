export const SLICES_NAMES = {
    USER: 'USER',
    REPOSITORIES: 'REPOSITORIES',
    EXECUTOR: 'EXECUTOR',
    ONE_REPOSITORY: 'ONE_REPOSITORY',
    TASKS: 'TASKS',
    NEXUS: 'NEXUS',
    INTEGRATIONS: 'INTEGRATIONS'
}

export const ACTION_TYPES = {
    USER_INFO_GET: 'user-info/get',
    REPOSITORIES_ALL_GET: 'repositories/all/get',
    REPOSITORY_BY_ID_GET: 'repositories/by-id/get',
    REPOSITORY_CREATE_BRANCH_POST: 'repositories/create-branch/post',
    TASKS_TEAM_GET: 'tasks/by-team/get',
    TASKS_ALL_GET: 'tasks/all/get',
    TASKS_CREATE_POST: 'tasks/create/post',
    TASKS_SELECT_ONE_POST: 'tasks/select/one/post',

    ARTIFACTS_ALL_GET: 'nexus/all/get',
    STANDS_GET_BY_TEAM: 'nexus/stands/by-id/get',
    STANDS_CREATE_POST: 'nexus/stands/create/post',

    EXECUTOR_RUN_INSTALL: 'executor/install',
    EXECUTOR_INSTALL_STAGES_GET: 'executor/install/stages/get',
    EXECUTOR_INSTALL_STATUS_GET: 'executor/install/status/get',

    EXECUTOR_RUN_BUILD: 'executor/build',
    EXECUTOR_BUILD_STAGES_GET: 'executor/build/stages/get',
    EXECUTOR_STATUS_GET: 'executor/status/get',
    EXECUTOR_STATUS_GET_BY_TEAM: 'executor/status/get-by-team',
    EXECUTOR_ARTIFACT_NUMBER_GET: 'executor/artifact-number/get',

    INTEGRATIONS_TEAM_STANDS_GET: 'integrations/team-stands/get',
    INTEGRATION_TEAMS_GET: 'integrations/teams/get',
    INTEGRATION_TYPES_GET: 'integrations/types/get',

}