import { createSlice } from '@reduxjs/toolkit'

import { SLICES_NAMES } from '../action-types'
import { integrationActions } from '../actions'

const initialState = {
    types: [],
    teams: []
}

export const integrationSlice = createSlice({
    name: SLICES_NAMES.INTEGRATIONS,
    initialState,
    extraReducers: (builder) => {
        builder
            .addCase(integrationActions.getIntegrationTypes.fulfilled, (state, action) => {
                state.types = action.payload
            })

            .addCase(integrationActions.getTeamsList.fulfilled, (state, action) => {
                state.teams = action.payload
            })
    }
})

export default integrationSlice.reducer
