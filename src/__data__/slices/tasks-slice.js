import { createSlice } from '@reduxjs/toolkit'

import { SLICES_NAMES } from '../action-types'
import { tasksActions } from '../actions'

const initialState = {
    list: [],
    teamList: []
}

export const tasksSlice = createSlice({
    name: SLICES_NAMES.TASKS,
    initialState,
    extraReducers: (builder) => {
        builder
            .addCase(tasksActions.getTeamTasks.fulfilled, (state, action) => {
                state.teamList = action.payload
            })
            .addCase(tasksActions.getAllTasks.fulfilled, (state, action) => {
                state.list = action.payload
            })
            .addCase(tasksActions.selectTask.fulfilled, (state, { payload }) => {
                state.list = state.list.map((task) => task.taskId === payload.taskId ? payload : task)
            })
    }
})

export default tasksSlice.reducer
