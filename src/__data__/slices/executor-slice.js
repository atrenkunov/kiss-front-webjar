import { createSlice } from '@reduxjs/toolkit'

import { SLICES_NAMES } from '../action-types'
import { executorActions } from '../actions'

const initialState = {
    allBuilds: [],
    installationStages: [],
    buildStages: []
}

export const executorSlice = createSlice({
    name: SLICES_NAMES.EXECUTOR,
    initialState,
    extraReducers: (builder) => {
        builder
            .addCase(executorActions.getActiveBuildsByTeamId.fulfilled, (state, action) => {
                state.allBuilds = action.payload
            })
            .addCase(executorActions.getInstallationStages.fulfilled, (state, action) => {
                state.installationStages = action.payload
            })
            .addCase(executorActions.getBuildStages.fulfilled, (state, action) => {
                state.buildStages = action.payload
            })
    }
})

export default executorSlice.reducer
