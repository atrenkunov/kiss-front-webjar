import user from './user-slice'
import repositories from './repositories-slice'
import executor from './executor-slice'
import tasks from './tasks-slice'
import nexus from './nexus-slice'
import integrations from './integration-slice'

export {
    user,
    repositories,
    executor,
    tasks,
    nexus,
    integrations
}

