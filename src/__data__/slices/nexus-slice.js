import { createSlice } from '@reduxjs/toolkit'

import { SLICES_NAMES } from '../action-types'
import { executorActions, nexusActions } from '../actions'

const initialState = {
    tasks: [],
    stands: []
}

export const nexusSlice = createSlice({
    name: SLICES_NAMES.NEXUS,
    initialState,
    extraReducers: (builder) => {
        builder
            .addCase(nexusActions.getAllArtifacts.fulfilled, (state, action) => {
                state.tasks = action.payload
            })
            .addCase(nexusActions.getStandsList.fulfilled, (state, action) => {
                state.stands = action.payload
            })
            .addCase(executorActions.postInstallOnStand.fulfilled, (state, action) => {
                const { id } = action.payload

                state.stands = state.stands.map((stand) => stand.id === id ? ({ ...action.payload  }) : stand)
            })
            .addCase(executorActions.getInstallStatus.fulfilled, (state, action) => {
                const { standIp, progress: buildInfo = {} } = action.payload
                
                state.stands = state.stands.map((stand) => stand.ip === standIp ? ({
                    ...stand,
                    ...action.payload,
                }) : stand)
            })
    }
})

export default nexusSlice.reducer
