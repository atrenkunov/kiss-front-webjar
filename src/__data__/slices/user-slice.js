import { createSlice } from '@reduxjs/toolkit'

import { SLICES_NAMES } from '../action-types'
import { userActions } from '../actions'

const initialState = {
    token: sessionStorage.getItem('auth-token'),
    status: 'idle',

    userInfo: {
        userId: '',
        firstName: '',
        lastName: '',
        team: {}
    },
    
    teams: []
}

export const userSlice = createSlice({
    name: SLICES_NAMES.USER,
    initialState,
    extraReducers: (builder) => {
        builder
            .addCase(userActions.getUserInfo.fulfilled, (state, action) => {
                state.userInfo = action.payload
            })
    }
})

export default userSlice.reducer
