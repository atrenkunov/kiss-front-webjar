import { createSlice } from '@reduxjs/toolkit'

import { SLICES_NAMES } from '../action-types'
import { reposActions } from '../actions'

const initialState = {
    list: [],
    currentRepo: {},
    qgModalBranchInfo: null
}

export const repositoriesSlice = createSlice({
    name: SLICES_NAMES.REPOSITORIES,
    initialState,
    reducers: {
        setQualityGatesModalBranch: (state, action) => {
            state.qgModalBranchInfo = action.payload;
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(reposActions.getAllRepositories.fulfilled, (state, action) => {
                state.list = action.payload
            })
            .addCase(reposActions.getRepository.pending, (state) => {
                state.currentRepo = {}
            })
            .addCase(reposActions.getRepository.fulfilled, (state, action) => {
                state.currentRepo = action.payload
            })
    }
})

export const { setQualityGatesModalBranch } = repositoriesSlice.actions

export default repositoriesSlice.reducer
