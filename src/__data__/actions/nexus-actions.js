import { createAsyncThunk } from '@reduxjs/toolkit'

import { NEXUS_API } from '../api-map'
import { ACTION_TYPES } from '../action-types'
import { handleAPIError } from '../../utils'

export const getAllArtifacts = createAsyncThunk(
    ACTION_TYPES.ARTIFACTS_ALL_GET,
    async (teamId, { rejectWithValue }) => {
        try {
            const response = await NEXUS_API.getArtifacts(teamId)
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)

export const getStandsList = createAsyncThunk(
    ACTION_TYPES.STANDS_GET_BY_TEAM,
    async ({ standType, teamId }, { rejectWithValue }) => {
        try {
            const response = await NEXUS_API.getStands(standType, teamId)
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)

export const createStand = createAsyncThunk(
    ACTION_TYPES.STANDS_CREATE_POST,
    async (data, { rejectWithValue }) => {
        try {
            const response = await NEXUS_API.createStand(data)
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)
