import { createAsyncThunk } from '@reduxjs/toolkit'

import { INTEGRATION_API, TEAM_API } from '../api-map'
import { ACTION_TYPES } from '../action-types'
import { handleAPIError } from '../../utils'

export const getIntegrationTypes = createAsyncThunk(
    ACTION_TYPES.INTEGRATION_TYPES_GET,
    async (_, { rejectWithValue }) => {
        try {
            const response = await INTEGRATION_API.getIntegrationTypes()
            console.log(24, response.data)
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)

export const getTeamsList = createAsyncThunk(
    ACTION_TYPES.INTEGRATION_TEAMS_GET,
    async (data, { rejectWithValue }) => {
        try {
            const response = await TEAM_API.getAllTeams()
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }

)
