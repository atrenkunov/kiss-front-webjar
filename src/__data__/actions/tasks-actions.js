import { createAsyncThunk } from '@reduxjs/toolkit'

import { JIRA_API } from '../api-map'
import { ACTION_TYPES } from '../action-types'
import { handleAPIError } from '../../utils'


export const getTeamTasks = createAsyncThunk(
    ACTION_TYPES.TASKS_TEAM_GET,
    async (teamId, { rejectWithValue }) => {
        try {
            const response = await JIRA_API.getTeamTasks(teamId)
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)

export const getAllTasks = createAsyncThunk(
    ACTION_TYPES.TASKS_ALL_GET,
    async (_, { rejectWithValue }) => {
        try {
            const response = await JIRA_API.getTasks()
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)

export const selectTask = createAsyncThunk(
    ACTION_TYPES.TASKS_SELECT_ONE_POST,
    async (data, { rejectWithValue }) => {
        try {
            const response = await JIRA_API.selectTask(data)
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)


export const createTask = createAsyncThunk(
    ACTION_TYPES.TASKS_CREATE_POST,
    async (data, { rejectWithValue }) => {
        try {
            const response = await JIRA_API.createTask(data)
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)
