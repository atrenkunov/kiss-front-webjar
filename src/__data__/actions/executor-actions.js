import { createAsyncThunk } from '@reduxjs/toolkit'

import { EXECUTOR_API, NEXUS_API } from '../api-map'
import { ACTION_TYPES } from '../action-types'
import { handleAPIError } from '../../utils'
import { EXECUTOR_STAGES_TYPES } from '../constants'

export const getActiveBuildsByTeamId = createAsyncThunk(
    ACTION_TYPES.EXECUTOR_STATUS_GET_BY_TEAM,
    async (teamId, { rejectWithValue }) => {
        try {
            const response = await EXECUTOR_API.getActiveBuildsByTeamId(teamId)
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)

export const getStatus = createAsyncThunk(
    ACTION_TYPES.EXECUTOR_STATUS_GET,
    async (buildId, { rejectWithValue }) => {
        try {
            const response = await EXECUTOR_API.getStatus(buildId)
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)

export const getArtifactNumber = createAsyncThunk(
    ACTION_TYPES.EXECUTOR_ARTIFACT_NUMBER_GET,
    async (buildId, { rejectWithValue }) => {
        try {
            const response = await EXECUTOR_API.getArtifactNumber(buildId)
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)

export const postInstallOnStand = createAsyncThunk(
    ACTION_TYPES.EXECUTOR_RUN_INSTALL,
    async (data, { rejectWithValue }) => {
        const { payload } = data
        try {
            const response = await NEXUS_API.install(payload)
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)

export const getInstallationStages = createAsyncThunk(
    ACTION_TYPES.EXECUTOR_INSTALL_STAGES_GET,
    async (data, { rejectWithValue }) => {
        try {
            const response = await EXECUTOR_API.getStages(EXECUTOR_STAGES_TYPES.INSTALLATION)
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)

export const getBuildStages = createAsyncThunk(
    ACTION_TYPES.EXECUTOR_BUILD_STAGES_GET,
    async (data, {rejectWithValue}) => {
        try {
            const response = await EXECUTOR_API.getStages(EXECUTOR_STAGES_TYPES.BUILD)
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)

export const getInstallStatus = createAsyncThunk(
    ACTION_TYPES.EXECUTOR_STATUS_GET,
    async ({ installId, standIp }, { rejectWithValue }) => {
        try {
            const response = await EXECUTOR_API.installStatus(installId)
            return {
                progress: response.data,
                standIp
            }
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)
