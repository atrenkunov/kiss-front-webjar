import { createAsyncThunk } from '@reduxjs/toolkit'

import { USER_API } from '../api-map'
import { ACTION_TYPES } from '../action-types'
import { handleAPIError } from '../../utils'

export const getUserInfo = createAsyncThunk(
    ACTION_TYPES.USER_INFO_GET,
    async (data, { rejectWithValue }) => {
        try {
            const response = await USER_API.info(data)
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)
