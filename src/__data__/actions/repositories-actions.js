import { createAsyncThunk } from '@reduxjs/toolkit'

import { REPOS_API } from '../api-map'
import { ACTION_TYPES } from '../action-types'
import { handleAPIError } from '../../utils'

export const getAllRepositories = createAsyncThunk(
    ACTION_TYPES.REPOSITORIES_ALL_GET,
    async (teamId, { rejectWithValue }) => {
        try {
            const response = await REPOS_API.getAll(teamId)
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)


export const getRepository = createAsyncThunk(
    ACTION_TYPES.REPOSITORY_BY_ID_GET,
    async (id, { rejectWithValue }) => {
        try {
            const response = await REPOS_API.getRepo(id)
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)


export const createBranch = createAsyncThunk(
    ACTION_TYPES.REPOSITORY_CREATE_BRANCH_POST,
    async (userData, { rejectWithValue }) => {
        const { repoId, data } = userData
        try {
            const response = await REPOS_API.createBranch(repoId, data)
            return response.data
        } catch (error) {
            return rejectWithValue(handleAPIError(error))
        }
    }
)
