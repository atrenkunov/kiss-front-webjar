import { createSelector } from 'reselect'

import { teamSelector } from './user-selector'

const integrationSelector = (state) => state.integrations

export const teamListSelector = createSelector(
    [integrationSelector],
    (state) => state.teams
)

export const otherTeamsSelector = createSelector(
    [teamListSelector, teamSelector],
    (teams, { id }) => teams.filter((team) => team.id !== id)
)

export const typesSelector = createSelector(
    [integrationSelector],
    (integration) => integration.types
)
