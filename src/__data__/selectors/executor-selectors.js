import { createSelector } from 'reselect'

import { repositoriesSelectors } from '.'

const executorSelector = (state) => state.executor

export const allBuildsSelector = createSelector(
    [executorSelector],
    (executor) => executor.allBuilds
)

export const activeBuildsSelector = createSelector(
    [executorSelector, repositoriesSelectors.currentRepoSelector],
    (executor, currentRepo) => executor.allBuilds.filter((build) => build.repositoryId === currentRepo.id)
)

export const installationStagesSelector = createSelector(
    [executorSelector],
    (executor) => executor.installationStages
)

export const buildStagesSelector = createSelector(
    [executorSelector],
    (executor) => executor.buildStages
)
