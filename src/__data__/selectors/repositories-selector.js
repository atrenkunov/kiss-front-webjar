import { createSelector } from 'reselect'

const repositoriesSelector = (state) => state.repositories

export const allReposSelector = createSelector(
    [repositoriesSelector],
    (state) => state.list
)

export const currentRepoSelector = createSelector(
    [repositoriesSelector],
    (state) => state.currentRepo
)

export const currentRepoBranchesSelector = createSelector(
    [currentRepoSelector],
    (repository) => repository.branchList?.map((branch) => ({
        name: branch.id,
        id: branch.id
    })) || []
)
