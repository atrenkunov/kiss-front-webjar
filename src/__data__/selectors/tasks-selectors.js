import { createSelector } from 'reselect'

const tasksSelector = (state) => state.tasks

export const tasksListSelector = createSelector(
    [tasksSelector],
    (state) => state.teamList
)

export const allTasksSelector = createSelector(
    [tasksSelector],
    (state) => state.list
)