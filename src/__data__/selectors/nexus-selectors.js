import { createSelector } from 'reselect'

import { beautify } from '../../utils'
import { HTML_ENTITIES } from '../constants'

const nexusSelector = (state) => state.nexus

export const nexusTasksSelector = createSelector(
    [nexusSelector],
    (state) => state.tasks
)

export const standsSelector = createSelector(
    [nexusSelector],
    (state) => state.stands
)

export const standTasksSelectors = createSelector(
    [nexusTasksSelector],
    (tasks) => tasks.map((artifact) => ({
        id: artifact.id,
        title: artifact.id,
        description: artifact.id ? artifact.name || beautify(HTML_ENTITIES.EM_DASH) : 'Без задачи'
    }))
)
