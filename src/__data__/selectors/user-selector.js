import { createSelector } from 'reselect'

const userSelector = (state) => state.user

export const infoSelector = createSelector(
    [userSelector],
    (state) => state.userInfo
)

export const teamSelector = createSelector(
    [infoSelector],
    (user) => user.team
)
