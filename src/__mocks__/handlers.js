import { handlersAuth } from './handlersAuth'
import { handlersExecutor } from './handlersExecutor'
import { handlersRepo } from './handlersRepo'

export const handlers = [
    ...handlersAuth,
    ...handlersRepo,
    ...handlersExecutor
]
