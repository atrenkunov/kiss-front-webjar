import { rest } from 'msw'

export const handlersRepo = [
    rest.get('/api/repo/:repoId', (req, res, ctx) => res(
        ctx.status(200),
        ctx.json({
            id: 'repo1',
            name: 'Репозиторий back разработчика',
            link: 'https://bitbucket.org/atrenkunov/repo1',
            branchList: [{
                repositoryId: 'repo1',
                id: 'develop',
                lastCommit: 'fnjksdfnalkdjqwe2jieooqwk',
                moduleVersion: '0.1.0',
                lastBuild: {
                    id: '0.1.0.125',
                    date: '22.09.2021',
                    author: 'Petya',
                    commit: 'sdjsodjoewoomo',
                    validation: {
                        status: 'OK',
                        checkList: [{
                            id: 'unit_tests',
                            status: 'OK',
                            link: 'http://junit.ru/check/repo1/branch/develop/info'
                        }, {
                            id: 'sonar',
                            status: 'OK',
                            link: 'http://sonarqube.ru/check/repo1/branch/develop/info'
                        }, {
                            id: 'oss',
                            status: 'OK',
                            link: 'http://oss.ru/check/repo1/branch/develop/info'
                        }
                        ]
                    }
                }
            }, {
                repositoryId: 'repo1',
                id: 'feature/GKR-123',
                lastCommit: 'mlfnwelkfnew;fmew;lfm',
                moduleVersion: '0.2.0',
                lastBuild: null
            }
            ]
        })
    )),
    rest.get('/api/repo/all/byTeam', (req, res, ctx) => res(
        ctx.status(200),
        ctx.json([
            {
                id: 'repo1',
                name: 'Репозиторий back разработчика',
                link: 'https://bitbucket.org/atrenkunov/repo1'
            },
            {
                id: 'repo2',
                name: 'Репозиторий front разработчика',
                link: 'https://bitbucket.org/atrenkunov/repo2'
            }
        ])
    )),
    rest.post('/api/repo/:repoId/branch/:branchId', (req, res, ctx) => res(
        ctx.status(200),
        ctx.json({
            repositoryId: 'repo1',
            id: 'feature/GKR-123',
            lastCommit: 'mlfnwelkfnew;fmew;lfm',
            moduleVersion: '0.2.0',
            lastBuild: null
        }),
    )),
    rest.delete('api/repo/:repoId/branch/:branchId', (req, res, ctx) => res(
        ctx.status(200)
    ))
]