import { rest } from 'msw'
export const handlersExecutor = [
    rest.get('/api/executor/build/stage/all', (req, res, ctx) => res(
        ctx.status(200),
        ctx.json([
          {
            "id": "download_sources",
            "name": "Скачиваю ветку из репозитория"
          },
          {
            "id": "compile",
            "name": "Компиляция программы"
          },
          {
            "id": "unit_tests",
            "name": "Запуск unit тестов"
          },
          {
            "id": "sonar",
            "name": "Запуск сонара"
          },
          {
            "id": "oss",
            "name": "Запуск oss"
          },
          {
            "id": "publish",
            "name": "Публикация артефактов"
          }
        ]),
    )),
    rest.post('/api/executor/build', (req, res, ctx) => res(
        ctx.status(200),
        ctx.json({
            "buildId": 125 
        }),
    )),
    rest.get('/api/executor/build/:buildId/status', (req, res, ctx) => res(
        ctx.status(200),
        ctx.json({
          "createTimestamp": "2021-09-30T15:26:02.311+03:00",
          "id": "1",
          "type": "BUILD",
          "repositoryId": "repo1",
          "branchId": "develop",
          "moduleVersion": "unknown",
          "state": "WAITING",
          "stages": [
            {
              "id": "download_sources",
              "description": "Stage id=download_sources completed successfully",
              "state": "COMPLETED",
              "progress": 100
            },
            {
              "id": "compile",
              "description": "Stage id=compile completed successfully",
              "state": "COMPLETED",
              "progress": 100
            },
            {
              "id": "unit_tests",
              "description": "Stage id=unit_tests completed successfully",
              "state": "COMPLETED",
              "progress": 100
            },
            {
              "id": "sonar",
              "description": "Stage id=sonar completed successfully",
              "state": "COMPLETED",
              "progress": 100
            },
            {
              "id": "publish",
              "description": "Stage id=publish completed successfully",
              "state": "WAITING",
              "progress": 100
            }
          ]
        })
    )),
    rest.get('/api/executor/build/all/byTeam', (req, res, ctx) => res(
        ctx.status(200),
        ctx.json([
            {
              "createTimestamp": "2021-09-30T15:26:02.311+03:00",
              "id": "1",
              "type": "BUILD",
              "repositoryId": "repo1",
              "branchId": "develop",
              "moduleVersion": "unknown",
              "state": "BUILDING",
              "stages": [
                {
                  "id": "download_sources",
                  "description": "Stage id=download_sources completed successfully",
                  "state": "COMPLETED",
                  "progress": 100
                },
                {
                  "id": "compile",
                  "description": "Stage id=compile completed successfully",
                  "state": "COMPLETED",
                  "progress": 100
                },
                {
                  "id": "unit_tests",
                  "description": "Stage id=unit_tests completed successfully",
                  "state": "COMPLETED",
                  "progress": 100
                },
                {
                  "id": "sonar",
                  "description": "Stage id=sonar completed successfully",
                  "state": "COMPLETED",
                  "progress": 100
                },
                {
                  "id": "publish",
                  "description": "Stage id=publish completed successfully",
                  "state": "COMPLETED",
                  "progress": 100
                }
              ]
            },
            {
              "createTimestamp": "2021-09-30T15:27:18.504+03:00",
              "id": "2",
              "type": "BUILD",
              "repositoryId": "repo123",
              "branchId": "origin/master",
              "moduleVersion": "0.1",
              "state": "BUILDING",
              "stages": [
                {
                  "id": "download_sources",
                  "description": "Stage id=download_sources completed successfully",
                  "state": "COMPLETED",
                  "progress": 100
                },
                {
                  "id": "compile",
                  "description": "Stage id=compile completed successfully",
                  "state": "COMPLETED",
                  "progress": 100
                },
                {
                  "id": "unit_tests",
                  "description": "Stage id=unit_tests completed successfully",
                  "state": "COMPLETED",
                  "progress": 100
                },
                {
                  "id": "sonar",
                  "description": "Stage id=sonar completed successfully",
                  "state": "COMPLETED",
                  "progress": 100
                },
                {
                  "id": "publish",
                  "description": "Stage id=publish completed successfully",
                  "state": "COMPLETED",
                  "progress": 100
                }
              ]
            }
          ]),
    ))
]
