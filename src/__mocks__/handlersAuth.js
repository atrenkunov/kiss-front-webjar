import { rest } from 'msw'

export const handlersAuth = [
    rest.get('/api/auth/info', (req, res, ctx) => res(
        ctx.status(200),
        ctx.json({
            userId: 'u332141',
            firstName: 'Anton',
            lastName: 'Ivanov',
            team: {
                id: 'c121312',
                name: 'СБОЛ ПРО',
                owner: 'Petrov Ivan',
                phone: '8-123-321-11-00'
            }
        })
    ))
]