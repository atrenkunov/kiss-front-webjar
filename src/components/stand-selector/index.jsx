import React from 'react'
import { SegmentedGroup, SegmentedRadio } from '@sbol/design-system'

export const StandsSelector = ({ items, size = 'md', value = false, onChange }) => (
    <SegmentedGroup size={size} name="Выбор стенда">
        {items.map((stand) => (
            <SegmentedRadio
                key={stand.value}
                value={stand.value}
                name={stand.title}
                onChange={onChange}
                checked={stand.value === value}
            >
                {stand.title}
            </SegmentedRadio>
        ))}
    </SegmentedGroup>
)
