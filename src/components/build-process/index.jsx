import React, { useEffect, useState, useRef } from 'react'
import { Table, TableRow, TableCell, TableCellHead, Body2 } from '@sbol/design-system'

import { EXECUTOR_API } from '../../__data__/api-map'
import BuildArtifact from '../build-artifact'
import {EXECUTOR_STAGES_TYPES, HTML_ENTITIES} from '../../__data__/constants'
import {beautify} from "../../utils";

const BuildProcess = ({ branchBuildId }) => {
    const [branchBuildStages, setBranchBuildStages] = useState({})
    const [currentStatus, setCurrentStatus] = useState()
    const timer = useRef(null)

    const receiveStatus = () => {
        EXECUTOR_API.getStatus(branchBuildId).then(({ data }) => {
            setCurrentStatus(data)
            if (data.state !== 'COMPLETED') {
                timer.current = setTimeout(receiveStatus, 2000)
            }
        })
    }

    useEffect(() => {
        EXECUTOR_API.getStages(EXECUTOR_STAGES_TYPES.BUILD).then(({ data }) => setBranchBuildStages(
            data.reduce((acc, cur) => {
                acc[cur.id] = cur.name
                return acc
            }, {})
        ))
        receiveStatus()
        return () => {
            clearTimeout(timer.current)
        }
    }, [])

    if (!currentStatus) {
        return 'Загрузка...'
    }
  
    return (
        <Table>
            <TableRow>
                <TableCellHead>Ветка</TableCellHead>
                <TableCell>
                    <Body2 verticalMargin="zero" horizontalMargin="open" horizontalMarginDirection="left">
                        {currentStatus.branchId}
                    </Body2>
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCellHead>Коммит</TableCellHead>
                <TableCell>
                    <Body2 verticalMargin="zero" horizontalMargin="open" horizontalMarginDirection="left">
                        {currentStatus.commit}
                    </Body2>
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCellHead>Версия модуля</TableCellHead>
                <TableCell>
                    <Body2 verticalMargin="zero" horizontalMargin="open" horizontalMarginDirection="left">
                        {currentStatus.moduleVersion}
                    </Body2>
                </TableCell>
            </TableRow>
            {currentStatus && currentStatus.stages.map((oneStage) => (
                <TableRow key={oneStage.id}>
                    <TableCellHead>{branchBuildStages[oneStage.id]}</TableCellHead>
                    <TableCell>
                        <Body2 verticalMargin="zero" horizontalMargin="open" horizontalMarginDirection="left">
                            {oneStage.progress}%
                        </Body2>
                    </TableCell>
                </TableRow>
            ))}
            <TableRow>
                <TableCellHead>Ссылка на артефакт</TableCellHead>
                <TableCell>
                    <Body2 verticalMargin="zero" horizontalMargin="open" horizontalMarginDirection="left">
                        {currentStatus.state === 'COMPLETED' ? <BuildArtifact buildId={currentStatus.id} /> : beautify(HTML_ENTITIES.EM_DASH)}
                    </Body2>
                </TableCell>
            </TableRow>
        </Table>
    )
}

export default BuildProcess
