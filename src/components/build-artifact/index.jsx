import { Link } from '@sbol/design-system'
import React, { useEffect, useState } from 'react'
import { EXECUTOR_API } from '../../__data__/api-map'

// todo: реализовать bulk api
const BuildArtifact = React.memo(({buildId}) => {
  const [artifact, setArtifact] = useState(null)

  useEffect(() => {
    EXECUTOR_API.getArtifactNumber(buildId).then(({data}) => {
      setArtifact(data)
    })
  }, [])

  return artifact ? <Link
    title={artifact.name}
    href={artifact.link}
    target="_blank"
  /> : 'n/a'
})

export default BuildArtifact