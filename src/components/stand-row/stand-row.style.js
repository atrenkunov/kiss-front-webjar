import styled from '@emotion/styled'
import { ButtonSecondary, LabeledValueSelect } from '@sbol/design-system'
import { css } from '@emotion/react'
import { paddingStyle } from '@sbol/design-system/core/indent-wrapper'

export const SelectStyled = styled(LabeledValueSelect)`
  width: 200px;
`

export const StatusButtonStyled = styled(ButtonSecondary)(({ theme, colorScheme }) => css`
  border-color: ${theme[`${colorScheme}Transparent40`]};
  cursor: default;
  
  ${paddingStyle({
        size: 'sm',
        verticalPadding: 'micro',
        horizontalPadding: 'micro',
  })}
  
  &:hover,
  &:focus,
  &:active {
    border-color: ${theme[`${colorScheme}Transparent40`]};
  }
  
  p {
    color: ${theme[colorScheme]}!important;
  }
`)
