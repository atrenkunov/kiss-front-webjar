import { useDispatch, useSelector } from 'react-redux'
import { useCallback } from 'react'

import { actions, selectors } from '../../__data__'

export const useStandRow = () => {
    const dispatch = useDispatch()
    const branchesByTask = useSelector(selectors.nexusSelectors.nexusTasksSelector)
    const tasksList = useSelector(selectors.nexusSelectors.standTasksSelectors)

    const handleInstallOnStand = useCallback((data) => dispatch(actions.executorActions.postInstallOnStand(data)), [dispatch])

    return {
        branchesByTask,
        tasksList,
        
        handleInstallOnStand
    }
}
