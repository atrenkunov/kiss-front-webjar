import React, { useCallback, useMemo, useState } from 'react'
import { ButtonPrimary, ButtonTertiary, LabeledTextField, MarginWrapper, ValueOption } from '@sbol/design-system'
import { useTranslation } from 'react-i18next'

import { Grid } from '../UI/grid'
import { useUserInfo } from '../../utils/use-user-info'

import { SelectStyled } from './stand-row.style'
import { useStandRow } from './use-stand-row'

export const InstallForm = ({ taskId, standId, onCancel }) => {
    const { t } = useTranslation()
    const { tasksList = [], branchesByTask = [], handleInstallOnStand } = useStandRow()
    const { info = {} } = useUserInfo()
    const [selectedTask, setSelectedTask] = useState(tasksList.find((task) => task.id === taskId)?.id)
    const [selectedBranch, setSelectedBranch] = useState('')
    const [selectedArtifact, setSelectedArtifact] = useState('')
    const [description, setDescription] = useState('')
    const [error, setError] = useState({})
    const [loading, setLoading] = useState(false)

    const branchList = useMemo(() => {
        const taskSource = branchesByTask.find((task) => task.id === selectedTask) || branchesByTask[0]
        return taskSource?.branches.map((branch) => ({
            id: branch.branchId,
            title: branch.branchId,
            ...branch
        }))
    }, [branchesByTask, selectedTask])

    const branchVersions = useMemo(() => {
        const branchSource = branchList.find((branch) => branch.id === selectedBranch) || branchList[0]
        return branchSource.artifacts?.map((artifact) => ({
            id: artifact.id,
            title: artifact.name,
            ...artifact
        }))
    }, [branchList, selectedBranch])

    const handleTaskChange = useCallback((event) => {
        setSelectedTask(event)
        setSelectedBranch('')
        setSelectedArtifact('')
        setError({})
    }, [])

    const handleBranchChange = useCallback((event) => {
        setSelectedBranch(event)
        setSelectedArtifact('')
        setError({})
    }, [])

    const handleArtifactSelect = useCallback((event) => {
        setSelectedArtifact(event)
        setError({})
    }, [])

    const handleDescriptionChange = useCallback((event) => {
        setDescription(event)
    }, [])

    const handleInstall = useCallback(async () => {
        if (selectedTask === null || selectedTask === undefined) {
            setError({
                selectedTask: t('error.empty.build.task')
            })
            return
        }
        if (!selectedBranch) {
            setError({
                selectedBranch: t('error.empty.build.branch')
            })
            return
        }
        if (!selectedArtifact) {
            setError({
                selectedArtifact: t('error.empty.build.version')
            })
            return
        }
        setLoading(true)
        const { type } = await handleInstallOnStand({
            payload: {
                owner: `${info.firstName} ${info.lastName}`,
                description,
                machineId: standId,
                artifactId: selectedArtifact
            },
        })
        if (type.includes('fulfilled')) {
            onCancel()
        }
        setLoading(false)
    }, [selectedTask, selectedBranch, selectedArtifact, description, standId, branchVersions])

    // todo: disable
    return (
        <Grid fullWidth size="h1" verticalMargin="open" verticalMarginDirection="bottom" align="flex-start">
            <SelectStyled
                label="Задача"
                horizontalMargin="inner"
                error={error.selectedTask}
                value={selectedTask}
                onChange={handleTaskChange}
            >
                {tasksList.map((task) => (
                    <ValueOption
                        key={task.id}
                        title={task.title}
                        value={task.id}
                        description={task.description}
                    />
                ))}
            </SelectStyled>
            <SelectStyled
                label="Ветка"
                horizontalMargin="inner"
                error={error.selectedBranch}
                value={selectedBranch}
                onChange={handleBranchChange}
            >
                {branchList.map((branch) => (
                    <ValueOption
                        key={branch.id}
                        title={branch.title}
                        value={branch.id}
                    />
                ))}
            </SelectStyled>
            <SelectStyled
                label="Версия"
                horizontalMargin="inner"
                error={error.selectedArtifact}
                value={selectedArtifact}
                onChange={handleArtifactSelect}
            >
                {branchVersions.map((version) => (
                    <ValueOption
                        key={version.id}
                        title={version.title}
                        value={version.id}
                    />
                ))}
            </SelectStyled>
            <LabeledTextField label="Комментарий к установке" value={description} onChange={handleDescriptionChange} />
            <MarginWrapper
                size="md"
                verticalMargin="open"
                verticalMarginDirection="top"
                horizontalMargin="open"
                horizontalMarginDirection="left"
            >
                <ButtonPrimary
                    verticalMargin="open"
                    loading={loading}
                    title={t('stands.page.version.build')}
                    onClick={handleInstall}
                />
                <ButtonTertiary
                    verticalMargin="open"
                    title={t('stands.page.version.cancel')}
                    onClick={onCancel}
                />
            </MarginWrapper>
        </Grid>
    )
}
