import React, { useCallback, useMemo, useState } from 'react'
import {
    Body2,
    ExternalLink,
    TableCell,
    TableRow,
    Link, ButtonTransparent, Tip, TooltipClick,
} from '@sbol/design-system'
import { useTranslation } from 'react-i18next'

import { Grid, StagesRow, StatusButton } from '../UI'
import { useStandsPage } from '../../pages/stands-page/use-stands-page'
import { getStagesTip } from '../../utils'

import { useStandRow } from './use-stand-row'
import { InstallForm } from './install-form'

const INSTALL_STATUSES = {
    COMPLETED: 'COMPLETED',
    WAITING: 'WAITING',
    RUNNING: 'RUNNING',
    SKIPPED: 'SKIPPED'
}

export const StandRow = ({ stand = {} }) => {
    const { t } = useTranslation()
    const { tasksList = [] } = useStandRow()
    const { installationStages = [] } = useStandsPage()

    const { installedArtifact = {}, installationProgress = {}, jiraTaskId = {} } = stand

    const { link = '', name = '' } = installedArtifact || {}
    const { taskId = '' } = jiraTaskId || {}
    const { stages = [], branchId = '—', state = 'N/A' } = installationProgress || {}

    const activeTask = useMemo(() => tasksList.find((task) => task.id === taskId) || { title: '—' }, [tasksList, taskId])

    const [showForm, setShowForm] = useState(false)

    const handleInstallForm = useCallback(() => {
        setShowForm(!showForm)
    }, [showForm])

    return (
        <>
            <TableRow key={stand.id}>
                <TableCell>{stand.ip}</TableCell>
                <TableCell>
                    <Grid direction="column">
                        <Body2 fontWeight="semibold" verticalMargin="zero">
                            {activeTask.title}
                        </Body2>
                        {activeTask.description && (
                            <Body2 colorScheme="secondary" verticalMargin="zero">
                                {activeTask.description}
                            </Body2>
                        )}
                    </Grid>
                </TableCell>
                <TableCell>{branchId}</TableCell>
                <TableCell>
                    <Grid direction="column">
                        {
                            name && (
                                <ExternalLink
                                    href={link}
                                    title={name}
                                    verticalMargin="nano"
                                    verticalMarginDirection="bottom"
                                />
                            )
                        }
                        <Link
                            title={t(`stands.page.version.${name ? 'change' : 'set'}`)}
                            verticalMargin="zero"
                            horizontalMargin="zero"
                            onClick={handleInstallForm}
                        />
                    </Grid>
                </TableCell>
                <TableCell>{stand.owner || '-'}</TableCell>
                <TableCell>
                    <Grid fullWidth size="lg" horizontalMargin="inner" horizontalMarginDirection="right">
                        {stand.description || '-'}
                    </Grid>
                </TableCell>
                <TableCell>
                    {
                        state === INSTALL_STATUSES.RUNNING ? (
                            <StagesRow items={stages} dictionary={installationStages} />
                        ) : (
                            <Grid justify="flex-start" align="center">
                                <StatusButton
                                    verticalPadding="micro"
                                    horizontalPadding="micro"
                                    verticalMargin="zero"
                                    horizontalMargin="nano"
                                    horizontalMarginDirection="right"
                                    title={state}
                                    status={state}
                                />
                                {
                                    state && state.toUpperCase() !== 'N/A' && (
                                        <TooltipClick>
                                            <ButtonTransparent
                                                verticalPadding="nano"
                                                horizontalPadding="nano"
                                                verticalMargin="zero"
                                                horizontalMargin="zero"
                                                iconName="icon:sberboard/common/ic24Info"
                                            />
                                            <Tip
                                                title={t('stages.tip.install')}
                                                description={getStagesTip(stages, installationStages)}
                                            />
                                        </TooltipClick>
                                    )
                                }
                            </Grid>
                        )
                    }
                </TableCell>
            </TableRow>
            {showForm && (
                <InstallForm
                    standId={stand.id}
                    taskId={jiraTaskId?.taskId}
                    onCancel={handleInstallForm}
                />
            )}
        </>
    )
}
