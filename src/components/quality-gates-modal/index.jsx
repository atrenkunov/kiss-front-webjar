import React, { useState, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { ButtonPrimary, ButtonSecondary, Checkbox } from '@sbol/design-system'

import { Grid, Popup } from '../UI'
import { teamSelector } from '../../__data__/selectors/user-selector'
import { EXECUTOR_API } from '../../__data__/api-map'
import { BUILD_LINK } from '../../__data__/constants'

const QUALITY_GATES = [
    { id: 'unit_tests', title: 'Run unit tests' },
    { id: 'sonar', title: 'SONAR' },
    { id: 'oss', title: 'SAST' }
]

const QualityGatesModal = ({ branchInfo, closeModal, repoInfo, getRepoInfo }) => {
    const [checkList, setCheckList] = useState({})
    const dispatch = useDispatch()
    const history = useHistory()
    const team = useSelector(teamSelector)
    const toggleCheckbox = (id) => {
        setCheckList({ ...checkList, [id]: !checkList[id] })
    }

    const buildBranch = useCallback(() => {
        EXECUTOR_API.build({
            teamId: team.id,
            repositoryId: repoInfo.id,
            branchId: branchInfo.id,
            checkList: Object.keys(checkList).filter((elt) => checkList[elt])
        }).then(({ data }) => {
            history.push(BUILD_LINK(data.id))
            dispatch(getRepoInfo(repoInfo.id))
        })
        closeModal()
    }, [repoInfo.id, team.id, branchInfo.id, checkList])

    return (
        <Popup onClose={closeModal} title="Доступные Quality Gates">
            <Grid direction="column" size="lg" verticalMargin="micro" verticalMarginDirection="top">
                {
                    QUALITY_GATES.map((elt) => (
                        <label key={elt.id}>
                            <Checkbox onChange={() => toggleCheckbox(elt.id)}>
                                {elt.title}
                            </Checkbox>
                        </label>
                    ))
                }
            </Grid>

            <Grid size="lg" verticalMargin="inner" verticalMarginDirection="top">
                <ButtonSecondary onClick={closeModal} title="Отмена" />
                <ButtonPrimary onClick={buildBranch} title="Собрать" />
            </Grid>
        </Popup>
    )
}

export default QualityGatesModal
