import React from 'react'
import { Tip, TooltipHover } from '@sbol/design-system'
import { useTranslation } from 'react-i18next'

import { Grid } from '../grid'

import { IconLoaderStyled } from './stages-row.style'
import {STAGE_DESCRIPTIONS} from "../../../__data__/constants";

// ic36Clock
// ic36CircleCross
// ic36Checkmark
// ic36Exclamation

const STAGE_ICONS = {
    RUNNING: 'ic36CounterClockwise',
    WAITING: 'ic36Clock',
    COMPLETED: 'ic36Checkmark',
    SKIPPED: 'ic36Exclamation',
    FAILED: 'ic36CircleCross'
}

const STAGE_COLOURS = {
    RUNNING: 'stageRunning',
    WAITING: 'secondary',
    COMPLETED: 'success',
    SKIPPED: 'tertiary',
    FAILED: 'warning'
}

export const StagesRow = ({
    dictionary = [],
    items = []
}) => {
    const { t } = useTranslation()

    return (
        <Grid>
            {
                items.map((stage) => (
                    <TooltipHover key={stage.id}>
                        <IconLoaderStyled
                            status={stage.state}
                            name={`icon:sberboard/common/${STAGE_ICONS[stage.state]}`}
                            colorScheme={STAGE_COLOURS[stage.state]}
                        />
                        <Tip
                            title={`${dictionary.find((word) => word.id === stage.id)?.name} (${STAGE_DESCRIPTIONS[stage.state]})`}
                            description={t('stages.row.progress', { progress: stage.progress })}
                        />
                    </TooltipHover>
                ))
            }
        </Grid>
        
    )
}
