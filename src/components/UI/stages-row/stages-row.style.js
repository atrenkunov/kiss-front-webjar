import { css, keyframes } from '@emotion/react'
import styled from '@emotion/styled'
import { IconLoader } from '@sbol/design-system'

const spin = keyframes`
    from {transform:rotate(360deg);}
    to {transform:rotate(0deg);}
`

export const IconLoaderStyled = styled(IconLoader)(({ status = '' }) => css`
    ${status === 'RUNNING' && css`
        animation: ${spin} 1s ease infinite;
    `}
`)
