import React from 'react'
import styled from '@emotion/styled'
import { Global, css } from '@emotion/react'
import { config } from '../../../constants/config'

export const ContentWrapper = styled.div`
  padding: 45px 45px 0  ${ p => p.isSideMenuOpened ? '390px;' : '85px' };
  transition: padding 0.5s;
`

export const GlobalStyle = () => {
    const extensionCss = css`
        min-width: 780px;
        min-height: 599px;
    `

    return <Global
        styles={css`
            @font-face {
                font-family: 'SBSans';
                font-display: swap;
                src: url('/fonts/regular/SBSansText-Regular.eot');
                src:
                        url('/fonts/regular/SBSansText-Regular.eot?#iefix') format('embedded-opentype'),
                        url('/fonts/regular/SBSansText-Regular.woff2') format('woff2'),
                        url('/fonts/regular/SBSansText-Regular.woff') format('woff'),
                        url('/fonts/regular/SBSansText-Regular.ttf') format('truetype'),
                        url('/fonts/regular/SBSansText-Regular.svg#SBSans') format('svg');
                font-weight: normal;
                font-style: normal;
            }
            
            @font-face {
                font-family: 'SBSans';
                font-display: swap;
                src: url('/fonts/medium/SBSansText-Medium.eot');
                src:
                        url('/fonts/medium/SBSansText-Medium.eot?#iefix') format('embedded-opentype'),
                        url('/fonts/medium/SBSansText-Medium.woff2') format('woff2'),
                        url('/fonts/medium/SBSansText-Medium.woff') format('woff'),
                        url('/fonts/medium/SBSansText-Medium.ttf') format('truetype'),
                        url('/fonts/medium/SBSansText-Medium.svg#SBSans') format('svg');
                font-weight: 500;
                font-style: normal;
            }
            
            @font-face {
                font-family: 'SBSans';
                font-display: swap;
                src: url('/fonts/semibold/SBSansText-Semibold.eot');
                src:
                        url('/fonts/semibold/SBSansText-Semibold.eot?#iefix') format('embedded-opentype'),
                        url('/fonts/semibold/SBSansText-Semibold.woff2') format('woff2'),
                        url('/fonts/semibold/SBSansText-Semibold.woff') format('woff'),
                        url('/fonts/semibold/SBSansText-Semibold.ttf') format('truetype'),
                        url('/fonts/semibold/SBSansText-Semibold.svg#SBSans') format('svg');
                font-weight: 600;
                font-style: normal;
            }
            
            @font-face {
                font-family: 'SBSans';
                font-display: swap;
                src: url('/fonts/bold/SBSansText-Bold.eot');
                src:
                        url('/fonts/bold/SBSansText-Bold.eot?#iefix') format('embedded-opentype'),
                        url('/fonts/bold/SBSansText-Bold.woff2') format('woff2'),
                        url('/fonts/bold/SBSansText-Bold.woff') format('woff'),
                        url('/fonts/bold/SBSansText-Bold.ttf') format('truetype'),
                        url('/fonts/bold/SBSansText-Bold.svg#SBSans') format('svg');
                font-weight: bold;
                font-style: normal;
            }
            
            @font-face {
                font-family: 'SBSansDisplay';
                font-display: swap;
                src: url('/fonts/display-regular/SBSansDisplay-Regular.eot');
                src:
                        url('/fonts/display-regular/SBSansDisplay-Regular.eot?#iefix') format('embedded-opentype'),
                        url('/fonts/display-regular/SBSansDisplay-Regular.woff2') format('woff2'),
                        url('/fonts/display-regular/SBSansDisplay-Regular.woff') format('woff'),
                        url('/fonts/display-regular/SBSansDisplay-Regular.ttf') format('truetype'),
                        url('/fonts/display-regular/SBSansDisplay-Regular.svg#SBSans') format('svg');
                font-weight: normal;
                font-style: normal;
            }
            
            @font-face {
                font-family: 'SBSansDisplay';
                font-display: swap;
                src: url('/fonts/display-semibold/SBSansDisplay-SemiBold.eot');
                src:
                        url('/fonts/display-semibold/SBSansDisplay-SemiBold.eot?#iefix') format('embedded-opentype'),
                        url('/fonts/display-semibold/SBSansDisplay-SemiBold.woff2') format('woff2'),
                        url('/fonts/display-semibold/SBSansDisplay-SemiBold.woff') format('woff'),
                        url('/fonts/display-semibold/SBSansDisplay-SemiBold.ttf') format('truetype'),
                        url('/fonts/display-semibold/SBSansDisplay-SemiBold.svg#SBSans') format('svg');
                font-weight: 600;
                font-style: normal;
            }
            
            html, body {
                ${ config.isExtension && extensionCss}
                font-family: SBSans,Arial,Helvetica,sans-serif;
            }
      `}
    />
}
