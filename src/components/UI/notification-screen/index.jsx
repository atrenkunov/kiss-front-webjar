import React from 'react'
import { TechnicalError } from '@sbol/design-system'

import { NotificationWrapper } from './notification-screen.style'

const PATH = '/assets/'

export const getImageUrl = (name) => `${PATH}${name}.png`
export const getSrcSet = (name) => `${PATH}${name}_2x.png 2x, ${PATH}${name}_3x.png 3x`


export const NotificationScreen = ({ title, description, imageName = 'disabled', children }) => (
    <NotificationWrapper>
        <TechnicalError
            imageSrc={getImageUrl(imageName)}
            srcSet={getSrcSet(imageName)}
            title={title}
            description={description}
        >
            {children}
        </TechnicalError>
    </NotificationWrapper>
)
