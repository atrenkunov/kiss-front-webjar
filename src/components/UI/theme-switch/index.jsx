import React, { useContext, useCallback } from 'react'

import { ThemeContext } from '../../../__data__/contexts/theme-context'
import { DARK_THEME, LIGHT_THEME } from '../../../__data__/constants'

export const ThemeSwitch = () => {
    const { theme, setTheme } = useContext(ThemeContext)

    const handleTheme = useCallback(() => {
        setTheme(theme === LIGHT_THEME ? DARK_THEME : LIGHT_THEME)
    }, [theme])

    return (
        <button onClick={handleTheme}>
            change theme
        </button>
    )
}
