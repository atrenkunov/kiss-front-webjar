import styled from '@emotion/styled'
import { css } from '@emotion/react'
import { mdShadow, smShadow } from '@sbol/design-system/core/styles/shadows.config.style'
import { smBorderRadius } from '@sbol/design-system/core/styles/radius.config.style'
import { paddingStyle, PaddingWrapper } from '@sbol/design-system/core/indent-wrapper'
import { sbolEase } from '@sbol/design-system/core/styles/animation-timing-functions'
import { baseX } from '@sbol/design-system/core/styles/semantic.config.style'

export const Container = styled.div(({ theme }) => css`
  cursor: pointer;
  border: 1px solid ${theme.elevationOneBorderNormal};
  background-color: ${theme.elevationOneBody};

  box-shadow: ${smShadow(theme)};
  border-radius: ${smBorderRadius};

  transition-property: transform, box-shadow, border-color;
  transition-duration: 0.15s;
  transition-timing-function: ${sbolEase};
  user-select: none;
  
  ${paddingStyle({
        size: 'lg',
        horizontalPadding: 'open',
    })}
  
  &:hover {
    box-shadow: ${mdShadow(theme)};
    border-color:${theme.elevationOneBorderHover};
    transform: translateY(-${baseX}px);
  }
`)

export const Content = styled(PaddingWrapper)`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
`
