import React, { useCallback } from 'react'
import { ExternalLink } from '@sbol/design-system'
import { useTranslation } from 'react-i18next'
import { Headline3, Typography } from '@sbol/design-system/core/typography'
import { useHistory } from 'react-router-dom'

import { REPOSITORY_LINK } from '../../../__data__/constants'

import { Container, Content } from './repository-card.style'

export const RepositoryCard = ({ item }) => {
    const { t } = useTranslation()
    const history = useHistory()
    const { id = '', name = '', link = '' } = item

    const handleClick = useCallback(() => {
        history.push(REPOSITORY_LINK(id))
    }, [id])

    const handleStopPropagation = useCallback((event) => {
        event.stopPropagation()
    }, [])

    return (
        <Container
            role="button"
            aria-label="Перейти к репозиторию"
            onClick={handleClick}
        >
            <Content size="lg" verticalPadding="open">
                <Headline3 indent="zero">{id}</Headline3>
                {name && (
                    <Typography
                        as="div"
                        size="md"
                        verticalMargin="micro"
                        colorScheme="secondary"
                    >
                        {name}
                    </Typography>
                )}
                <ExternalLink
                    onClick={handleStopPropagation}
                    href={link}
                    fontWeight="semibold"
                    title={t('repository.external.link')}
                />
            </Content>
        </Container>
    )
}
