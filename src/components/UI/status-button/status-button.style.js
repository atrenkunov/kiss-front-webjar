import styled from '@emotion/styled'
import { ButtonSecondary } from '@sbol/design-system'
import { css } from '@emotion/react'

export const StatusButtonStyled = styled(ButtonSecondary)(({ theme, colorScheme, onClick }) => css`
  border-color: ${theme[`${colorScheme}Transparent40`]};
  cursor: ${onClick ? 'pointer' : 'default'};
  
  &:hover,
  &:focus,
  &:active {
    border-color: ${theme[`${colorScheme}Transparent40`]};
  }
  
  p {
    color: ${theme[colorScheme]}!important;
  }
`)
