import React from 'react'

import { StatusButtonStyled } from './status-button.style'

const STATUS_COLOURS = {
    COMPLETED: 'success',
    WAITING: 'secondary',
    SKIPPED: 'tertiary',
    'N/A': 'tertiary',
}

export const StatusButton = ({ status = 'N/A', ...props }) => (
    <StatusButtonStyled
        title={status}
        horizontalMargin="zero"
        horizontalMarginDirection="both"
        colorScheme={STATUS_COLOURS[status.toUpperCase()]}
        {...props}
    />
)

