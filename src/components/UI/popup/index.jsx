import React, { useCallback } from 'react'
import { Headline3, IconLoader } from '@sbol/design-system'

import { beautify } from '../../../utils'

import { PopupOverlay, PopupContent, PopupTitleContainer, CloseIconContainer } from './popup.style'

export const Popup = ({ title, children, onClose }) => {
    const stopPropagationHandler = useCallback((event) => {
        event.stopPropagation()
    }, [])

    return (
        <PopupOverlay onClick={onClose}>
            <PopupContent onClick={stopPropagationHandler}>
                {title && (
                    <PopupTitleContainer size="lg" verticalMargin="nano" verticalMarginDirection="bottom">
                        <Headline3 indent="zero">{beautify(title)}</Headline3>
                        <CloseIconContainer
                            size="md"
                            horizontalMargin="inner"
                            horizontalMarginDirection="left"
                            onClick={onClose}
                        >
                            <IconLoader name="icon:sberboard/common/ic24Cross" />
                        </CloseIconContainer>
                    </PopupTitleContainer>
                )}
                {children}
            </PopupContent>
        </PopupOverlay>
    )
}
