import styled from '@emotion/styled'
import { css } from '@emotion/react'
import { MarginWrapper, paddingStyle } from '@sbol/design-system/core/indent-wrapper'
import { mdShadow } from '@sbol/design-system/core/styles/shadows.config.style'
import { mdBorderRadius } from '@sbol/design-system/core/styles/radius.config.style'

export const PopupOverlay = styled.div`
  position: fixed;
  z-index: 100;
  top:0;
  left: 0;
  width: 100vw;
  height: 100vh;
  background-color: ${({ theme }) => theme.additional16};
  display: flex;
  justify-content: center;
  align-items: center;
`

export const PopupContent = styled.div(({ theme }) => css`
  background-color: ${theme.whitePrimary};
  box-shadow: ${mdShadow(theme)};
  border-radius: ${mdBorderRadius};
  ${paddingStyle({
        size: 'lg',
        verticalPadding: 'open',
        horizontalPadding: 'open',
    })}
`)

export const PopupTitleContainer = styled(MarginWrapper)`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`

export const CloseIconContainer = styled(MarginWrapper)`
cursor: pointer;
`