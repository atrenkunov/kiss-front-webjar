import * as DsDarkTheme from '@sbol/design-system/core/styles/dark.theme.style.js'
import * as DsLightTheme from '@sbol/design-system/core/styles/light.theme.style.js'
import * as colors from '@sbol/design-system/core/styles/colors.config.style'

import { DARK_THEME, LIGHT_THEME } from '../../../__data__/constants'

export const lightTheme = {
    ...DsLightTheme,
    alphaLabel: colors.yellow15,
    alphaLabelFont: DsLightTheme.whitePrimary,
    stageRunning: colors.yellow1,
}

export const darkTheme = {
    ...DsDarkTheme,
    alphaLabel: colors.yellow15,
    alphaLabelFont: DsLightTheme.whitePrimary,
    stageRunning: colors.yellow1,
}

export const GLOBAL_THEME = {
    [LIGHT_THEME]: lightTheme,
    [DARK_THEME]: darkTheme
}
