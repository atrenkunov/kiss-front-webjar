import React, { useContext  } from 'react'
import { ThemeProvider } from '@emotion/react'

import { ThemeContext } from '../../../__data__/contexts/theme-context'
import { GLOBAL_THEME } from './theme'

export const ThemeWrapper = ({ children }) => {
    const { theme } = useContext(ThemeContext)

    return (
        <ThemeProvider
            theme={GLOBAL_THEME[theme]}
        >
            {children}
        </ThemeProvider>
    )
}
