import React from 'react'

import { GridStyled } from './grid.style'

export const Grid = ({
    fullWidth = false,
    direction = 'row',
    justify = 'flex-start',
    align = 'flex-start',
    wrap = '',
    children,
    ...props
}) => (
    <GridStyled
        fullWidth={fullWidth}
        justify={justify}
        align={align}
        direction={direction}
        wrap={wrap}
        {...props}
    >
        {children}
    </GridStyled>
)
