import styled from '@emotion/styled'
import { css } from '@emotion/react'
import { MarginWrapper } from '@sbol/design-system'

import { mediaSm, mediaMd, mediaLg, lgWidth, mdWidth, smWidth } from '../styles/media.config'

export const GridStyled = styled(MarginWrapper)(({
    direction = 'row',
    justify = 'flex-start',
    align = 'flex-start',
    wrap = false,
    fullWidth = false,
    horizontalMargin,
}) => css`
    display: flex;
    flex-direction: ${direction};
    justify-content: ${justify};
    align-items: ${align};
    flex: 1 1 auto;
    flex-wrap: ${wrap ? 'wrap' : 'nowrap'};
    
    ${!horizontalMargin && css`
        margin-left: auto;
        margin-right: auto;
    `}
  
    
    
    ${mediaLg} {
        max-width: ${lgWidth}px;
    }
    ${mediaMd} {
        max-width: ${mdWidth}px;
    }
    ${mediaSm} {
        max-width: ${smWidth}px;
    }
    
     ${fullWidth && css`
        width: 100%;
        max-width: 100%!important;
    `}
`)

