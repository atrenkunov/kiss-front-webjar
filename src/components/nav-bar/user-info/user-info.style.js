import styled from '@emotion/styled'
import { NavLink } from 'react-router-dom'
import { css } from '@emotion/react'
import { marginStyle, paddingStyle } from '@sbol/design-system/core/indent-wrapper'
import { smShadow } from '@sbol/design-system/core/styles/shadows.config.style'
import { smBorderRadius } from '@sbol/design-system/core/styles/radius.config.style'

export const UserData = styled(NavLink)(({ theme }) => css`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    position: relative;
    
    &:hover {
      opacity: 0.8;
    }
`)

export const InitialsContainer = styled.span(({ theme }) => css`
  background-color: ${theme.brand};
  border-radius: 50%;
  box-sizing: border-box;
  max-width: 50px;

  ${paddingStyle({
        size: 'sm',
        verticalPadding: 'inner',
        horizontalPadding: 'open'
    })}
  
  ${marginStyle({
        size: 'md',
        horizontalMargin: 'open',
        horizontalMarginDirection: 'left'
    })}
`)

export const NavBarContainer = styled.nav(({ theme }) => css`
  display: flex;
  flex-direction: column;
  background-color: ${theme.whitePrimary};
  box-shadow: ${smShadow(theme)};
  border-radius: ${smBorderRadius};
`)

export const UserInfoWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    position: relative;
    
    ${paddingStyle({
        size: 'lg',
        verticalPadding: 'micro'
    })}
`
