import React, { useEffect, useMemo } from 'react'
import { Body2, ButtonTransparent, MarginWrapper, Caption } from '@sbol/design-system'

import { getInitials } from '../../../utils'
import { APP_ROUTES } from '../../../__data__/constants'
import { useUserInfo } from '../../../utils/use-user-info'

import { UserData, InitialsContainer, NavBarContainer, UserInfoWrapper } from './user-info.style'

export const UserInfo = () => {
    const { info = {}, team = {}, fetchInfo } = useUserInfo()

    const userInitials = useMemo(() => getInitials([info.lastName, info.firstName]), [info.firstName, info.lastName])

    useEffect(() => {
        fetchInfo()
    }, [])

    return (
        <NavBarContainer>
            <UserInfoWrapper>
                <UserData to={APP_ROUTES.PROFILE}>
                    {
                        userInitials && (
                            <InitialsContainer>
                                <Body2
                                    indent="zero"
                                    verticalMargin="zero"
                                    colorScheme="whitePrimary"
                                    fontWeight="semibold"
                                >
                                    {userInitials}
                                </Body2>
                            </InitialsContainer>
                        )
                    }
                    <MarginWrapper
                        size="sm"
                        horizontalMargin="micro"
                        horizontalMarginDirection="left"
                    >
                        <Body2
                            indent="zero"
                            verticalMargin="zero"
                            fontWeight="semibold"
                        >
                            {info.lastName} {info.firstName}
                        </Body2>
                        {team.name && (
                            <Caption
                                indent="zero"
                                verticalMargin="zero"
                                colorScheme="secondary"
                            >
                                {team.name}
                            </Caption>
                        )}
                    </MarginWrapper>
                </UserData>
                <ButtonTransparent
                    iconReverse
                    iconName="icon:sberboard/common/ic24ArrowRightSquare"
                    verticalMargin="zero"
                    horizontalMargin="inner"
                    horizontalMarginDirection="right"
                />
            </UserInfoWrapper>
        </NavBarContainer>
    )
}
