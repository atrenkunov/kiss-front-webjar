import React, { useCallback, useState } from 'react'
import { Body1, IconLoader, MarginWrapper, Body2, Caption } from '@sbol/design-system'
import { IconLoaderViewBox } from '@sbol/design-system/core/icon/icon-view'
import { disableHandler } from '@sbol/design-system/core/utils/handlers'
import {useHistory} from 'react-router-dom'

import {
    NestedItemTopContent,
    NestedItemTitle,
    NewMenuItemIcon,
    NestedMenuCollapsedContentWrapper,
    NestedMenuCollapsedContent,
    NestedNavBarMenuItem,
    NestedMenuLink,
    NestedMenuDescription
} from './nav-bar.style'

export const NestedMenuItem = ({
    link,
    title,
    nestedMenu = [],
    createNew = false,
    onCreate
}) => {
    // todo: set open on route match
    const [isOpen, setIsOpen] = useState(false)
    const history = useHistory()

    const handleClick = useCallback((event) => {
        event.stopPropagation()
        setIsOpen(!isOpen)
    }, [isOpen])

    const handleCreateNewItem = useCallback((event) => {
        event.stopPropagation()
        onCreate()
    }, [onCreate])

    const handleLinkRedirect = useCallback(() => {
        history.push(link)
    }, [link])

    return (
        <NestedNavBarMenuItem
            role="button"
            onClick={handleLinkRedirect}
        >
            <NestedItemTopContent>
                <NestedItemTitle onClick={disableHandler(handleClick, !nestedMenu.length)}>
                    <Body1
                        indent="zero"
                        verticalMargin="zero"
                        horizontalMargin="open"
                        horizontalMarginDirection="left"
                    >
                        <NestedMenuLink
                            key={link}
                            to={link}
                        >
                            {title}
                        </NestedMenuLink>
                    </Body1>
                    {
                        !!nestedMenu.length && (
                            <IconLoader name={`icon:sberboard/common/${isOpen ? 'ic24ChevronUp' : 'ic24ChevronDown'}`} colorScheme="tertiary" />
                        )
                    }
                </NestedItemTitle>
                {
                    createNew && (
                        <MarginWrapper
                            size="md"
                            horizontalMargin="open"
                            horizontalMarginDirection="right"
                        >
                            <NewMenuItemIcon onClick={handleCreateNewItem}>
                                <IconLoaderViewBox name="icon:sberboard/common/ic24Plus" width="36" height="36" colorScheme="brandPrimary" />
                            </NewMenuItemIcon>
                        </MarginWrapper>
                    )
                }
            </NestedItemTopContent>
            <NestedMenuCollapsedContentWrapper isOpen={isOpen}>
                <NestedMenuCollapsedContent
                    size="md"
                    verticalPadding="nano"
                    verticalPaddingDirection="top"
                    horizontalPadding="open"
                    horizontalPaddingDirection="left"
                >
                    {nestedMenu.map((item) => (
                        <NestedMenuLink
                            key={item.link}
                            to={item.link}
                        >
                            <MarginWrapper
                                size="md"
                                horizontalMargin="nano"
                                horizontalMarginDirection="right"
                            >
                                <IconLoader name={`icon:sberboard/common/${item.icon}`} />
                            </MarginWrapper>
                            <NestedMenuDescription>
                                <Body2 verticalMargin="zero">
                                    {item.title}
                                </Body2>
                                <Caption colorScheme="tertiary" verticalMargin="nano">
                                    {item.description}
                                </Caption>
                            </NestedMenuDescription>
                        </NestedMenuLink>
                    ))}
                </NestedMenuCollapsedContent>
            </NestedMenuCollapsedContentWrapper>
        </NestedNavBarMenuItem>
    )
}
