import styled from '@emotion/styled'
import { css } from '@emotion/react'
import { NavLink } from 'react-router-dom'
import { smShadow } from '@sbol/design-system/core/styles/shadows.config.style'
import { smBorderRadius } from '@sbol/design-system/core/styles/radius.config.style'
import { marginStyle, paddingStyle, PaddingWrapper } from '@sbol/design-system/core/indent-wrapper'
import { IconLoaderViewBoxStyled } from '@sbol/design-system/core/icon/icon-view.style'

const transitionDuration = '0.17s'

export const NavBarPosition = styled.div`
  display: flex;
  flex-direction: column;
  transition: left 0.5s;
  position: fixed;
  left: ${ p => p.isSideMenuOpened ? '24px' : '-260px' };
  top: 24px;
  
  width: 300px;
`

export const NavBarMenu = styled.ul`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    flex-grow: 1;
`

export const NavBarMenuItem = styled(NavLink)(({ theme }) => css`
    position: relative;
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    width: 100%;
    user-select: none;
    
    &:not(:last-child) {
      border-bottom: 1px solid ${theme.elevationOneBorderNormal};
    }
    
     ${paddingStyle({
        size: 'md',
        verticalPadding: 'open',
    })}
    
    &:hover > p {
      transition: color ${transitionDuration} ease-in-out;
      color: ${theme.tertiary}
    }
`)

export const NestedNavBarMenuItem = styled.div(({ theme }) => css`
    cursor: pointer;
    position: relative;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
    width: 100%;
    user-select: none;

     &:not(:last-child) {
      border-bottom: 1px solid ${theme.elevationOneBorderNormal};
    }
    
     ${paddingStyle({
        size: 'md',
        verticalPadding: 'open',
    })}
`)

export const NestedItemTitle = styled.div(({ theme }) => css`
    cursor: pointer;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: flex-start;
    
    &:hover p {
      transition: color ${transitionDuration} ease-in-out;
      color: ${theme.tertiary}
    }
`)

export const NestedItemTopContent = styled.div(({ theme }) => css`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: 100%;
   
`)

export const NestedMenuCollapsedContentWrapper = styled.div(({ isOpen }) => css`
  max-height: 0px;
  width: 100%;
  transition: all ${transitionDuration} ease-in-out;
  overflow: hidden;
  visibility: hidden;
  
  ${isOpen && css`
    visibility: visible;
    max-height: 100%;
  `}
`)

export const NestedMenuCollapsedContent = styled(PaddingWrapper)`
    display: flex;
    flex-direction:column;
    justify-content: center;
    align-items: flex-start;
`

export const AlphaContainer = styled.div(({ theme }) => css`
    color: ${theme.alphaLabelFont};
    padding: 4px;
    background-color: ${theme.alphaLabel};
    border-radius: 4px;
    
    ${marginStyle({
        size: 'sm',
        horizontalMargin: 'micro',
        horizontalMarginDirection: 'left'
    })}
`)

export const LogoContainer = styled(NavLink)`
  display: flex;
  justify-content: center;
  align-items: center;
`

export const NavBarContainer = styled.nav(({ theme }) => css`
  display: flex;
  flex-direction: column;
  background-color: ${theme.whitePrimary};
  box-shadow: ${smShadow(theme)};
  border-radius: ${smBorderRadius};
`)

export const NestedMenuDescription = styled.div(({ theme }) => css`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  
  ${paddingStyle({
        size: 'md',
        verticalPadding: 'inner',
        verticalPaddingDirection: 'both'
    })};   
  
  &:hover {
    transition: background-color ${transitionDuration} ease-in-out;
    background-color: ${theme.backgroundOne};
  }
`)

export const NestedMenuLink = styled(NavLink)(({ theme }) => css`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  
  &:not(:last-child) {
      ${NestedMenuDescription} {
          flex-grow: 1;
          ${paddingStyle({
        size: 'md',
        verticalPadding: 'inner',
        verticalPaddingDirection: 'both'
    })};
          border-bottom: 1px solid ${theme.elevationOneBorderNormal};
      }
  }
`)


export const NewMenuItemIcon = styled.div(({ theme }) => css`
    cursor: pointer;
    &:hover {
        ${IconLoaderViewBoxStyled} {
            background-color: ${theme.brandTransparent8};
            border-radius: 50%;
        }
    }
    
    &:active {
        ${IconLoaderViewBoxStyled} {
            background-color: ${theme.brandTransparent16};
            border-radius: 50%;
        }
    }
`)
