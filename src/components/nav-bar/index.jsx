import React from 'react'
import { useTranslation } from 'react-i18next'
import { Icon, MarginWrapper, Body1 } from '@sbol/design-system'

import { APP_ROUTES } from '../../__data__/constants'

import {
    NavBarPosition,
    NavBarContainer,
    NavBarMenu,
    NavBarMenuItem,
    LogoContainer,
    AlphaContainer
} from './nav-bar.style'
import { UserInfo } from './user-info'
import { RepositoryMenu } from './repository-menu'

import { config } from '../../constants/config'

export const NavBar = ({isSideMenuOpened, setIsSideMenuOpened}) => {
    const { t } = useTranslation()
    return (

        <NavBarPosition
            isSideMenuOpened={isSideMenuOpened}
            onMouseEnter={() => setIsSideMenuOpened(true)}
            onMouseLeave={() => config.isExtension && setIsSideMenuOpened(false)}
        >
            {/* <MarginWrapper size="md" verticalMargin="open" verticalMarginDirection="bottom">
                <LogoContainer to={APP_ROUTES.MAIN_PAGE}>
                    <Icon icon={EasyLogo} />
                </LogoContainer>
            </MarginWrapper> */}
            <MarginWrapper size="md" verticalMargin="open" verticalMarginDirection="both">
                <UserInfo />
            </MarginWrapper>
            <NavBarContainer>
                <NavBarMenu>
                    <NavBarMenuItem to={APP_ROUTES.PLANNER}>
                        <Body1 indent="zero" verticalMargin="zero" horizontalMargin="open" horizontalMarginDirection="left">
                            {t('menu.planner')}
                        </Body1>
                    </NavBarMenuItem>
                    <NavBarMenuItem to={APP_ROUTES.TASKS}>
                        <Body1 indent="zero" verticalMargin="zero" horizontalMargin="open" horizontalMarginDirection="left">
                            {t('menu.tasks')}
                        </Body1>
                    </NavBarMenuItem>
                    <RepositoryMenu />
                    <NavBarMenuItem to={APP_ROUTES.BUILDS}>
                        <Body1 indent="zero" verticalMargin="zero" horizontalMargin="open" horizontalMarginDirection="left">
                            {t('menu.builds')}
                        </Body1>
                    </NavBarMenuItem>
                    <NavBarMenuItem to={APP_ROUTES.STANDS}>
                        <Body1 indent="zero" verticalMargin="zero" horizontalMargin="open" horizontalMarginDirection="left">
                            {t('menu.stands')}
                        </Body1>
                    </NavBarMenuItem>
                    <NavBarMenuItem to={APP_ROUTES.INTEGRATIONS}>
                        <Body1 indent="zero" verticalMargin="zero" horizontalMargin="open" horizontalMarginDirection="left">
                            {t('menu.integrations')}
                        </Body1>
                        <AlphaContainer>
                            {t('tag.alpha')}
                        </AlphaContainer>
                    </NavBarMenuItem>
                </NavBarMenu>
            </NavBarContainer>
        </NavBarPosition>
    )
}
