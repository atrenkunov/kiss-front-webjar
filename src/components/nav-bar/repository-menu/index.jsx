import React, {useCallback, useEffect, useMemo, useState} from 'react'
import { useTranslation } from 'react-i18next'
import {useHistory} from 'react-router-dom'

import { NestedMenuItem } from '../nested-item'
import { APP_ROUTES, REPOSITORY_LINK } from '../../../__data__/constants'
import { useRepositories } from '../../../pages/repositories-page/use-repositories'


export const RepositoryMenu = () => {
    const [popupOpen, setPopupOpen] = useState(false)
    const { teamInfo, list = [], getAllRepositories } = useRepositories()
    const { t } = useTranslation()
    const history = useHistory()
    useEffect(() => {
        if (teamInfo.id) {
            getAllRepositories(teamInfo.id)
        }
    }, [teamInfo.id])

    const handleRedirect = useCallback(() => {
        history.push(APP_ROUTES.CREATE_NEW_REPOSITORY)
    }, [popupOpen])

    const repositoriesSubmenu = useMemo(() => list.map((repo) => ({
        link: REPOSITORY_LINK(repo.id),
        title: repo.id,
        description: repo.name,
        icon: 'ic36DocumentOnDocument'
    })), [list])


    return (
        <>
            <NestedMenuItem
                createNew
                title={t('menu.repositories')}
                link={APP_ROUTES.MAIN_PAGE}
                nestedMenu={repositoriesSubmenu}
                onCreate={handleRedirect}
            />
        </>
    )
}
