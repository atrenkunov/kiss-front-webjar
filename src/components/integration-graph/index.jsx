import React, { useCallback } from 'react'
import { ResponsiveNetworkCanvas } from '@nivo/network'

const MOCK = {
    nodes: [
        {
            id: '1',
            radius: 8,
            depth: 1,
            color: 'rgb(97, 205, 187)',
            name: 'СБОЛ'
        },
        {
            id: '2',
            radius: 8,
            depth: 1,
            color: 'rgb(97, 205, 187)'
        },
        {
            id: '3',
            radius: 8,
            depth: 1,
            color: 'rgb(97, 205, 187)'
        },
        {
            id: '4',
            radius: 8,
            depth: 1,
            color: 'rgb(97, 205, 187)'
        },
        {
            id: '5',
            radius: 8,
            depth: 1,
            color: 'rgb(97, 205, 187)'
        },
        {
            id: '6',
            radius: 8,
            depth: 1,
            color: 'rgb(97, 205, 187)'
        },
        {
            id: '0',
            radius: 12,
            depth: 0,
            color: 'rgb(244, 117, 96)',
            name: 'СБОЛ ПРО (вы)'
        },
    ],
    links: [
        {
            source: '0',
            target: '1',
            distance: 50
        },
        {
            source: '0',
            target: '2',
            distance: 50
        },
        {
            source: '0',
            target: '3',
            distance: 50
        },
        {
            source: '0',
            target: '4',
            distance: 50
        },
        {
            source: '0',
            target: '5',
            distance: 50
        },
        {
            source: '0',
            target: '6',
            distance: 50
        },
    ]
}

export const IntegrationGraph = () => {
    console.log(5, 'integration')

    const customNodeRenderer = (ctx, {nodes, links}) => {
        console.log(ctx)
        MOCK.links.forEach((link) => {
            ctx.strokeStyle = getLinkColor(link)
            ctx.lineWidth = getLinkThickness(link)
            ctx.beginPath()
            ctx.moveTo(link.source.x, link.source.y)
            ctx.lineTo(link.target.x, link.target.y)
            ctx.stroke()
        })
    }

    return (
        <div style={{ height: '50vh', width: '50vw' }}>
            <ResponsiveNetworkCanvas
                nodes={MOCK.nodes}
                links={MOCK.links}
                isInteractive
                layers={[customNodeRenderer]}
                margin={{ top: 0, right: 0, bottom: 0, left: 0 }}
                linkDistance="distance"
                repulsivity={50}
                iterations={100}
                tooltip={useCallback((e) => {
                    console.log(e)
                    return <div>{e.name}</div>
                }, [])}
                nodeColor={useCallback((e) => e.color, [])}
                nodeBorderWidth={1}
                nodeBorderColor={{ theme: 'background' }}
            />
        </div>
    )
}
