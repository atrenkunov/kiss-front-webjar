import { auth, integrations } from './background-components'

chrome.runtime.onInstalled.addListener(() => {
    chrome.alarms.create('refresh', { periodInMinutes: 0.08 })
});

chrome.alarms.onAlarm.addListener((alarm) => {
    auth().then((user) => {
        checkIntegrations(user.team.id)
    })
});

function checkIntegrations(teamId) {
    integrations.updateAndGetChanges(teamId)
        .then((messages) => {
            messages.forEach((message) => {
                chrome.notifications.create('', {
                    title: 'Keep It Simple',
                    message: message.message,
                    iconUrl: '/favicon-16x16.png',
                    type: 'basic'
                })
            })
        })
}