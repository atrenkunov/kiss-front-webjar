import { INTEGRATION_API } from '../__data__/api-map'

const integrationsStorage = {
    getIntegrations: () => JSON.parse(localStorage.getItem('integrations')) || [],
    setIntegrations: (integrations) => localStorage.setItem('integrations', JSON.stringify(integrations))
}

const integrationsToHash = (integrations) => integrations.reduce(
    (result, elt) => {
        result[elt.id] = elt
        return result
    },
    {}
)

const isSomethingChanged = (oldIntegrations, newIntegrations) => {
    const result = []
    const newIntegrationsAsHash = integrationsToHash(newIntegrations)
    oldIntegrations.forEach((i) => {
        if (!newIntegrationsAsHash[i.id]) {
            return
        }
        if (i.status != newIntegrationsAsHash[i.id].status) {
            result.push({
                message: newIntegrationsAsHash[i.id].status !== 'OK' 
                    ? `Упал сервер ${i.integrationName}`
                    : `Сервер ${i.integrationName} поднялся`,
                integration: i
            })
        }
    })
    return result
}


const integrations = {
    updateAndGetChanges: async (teamId) => {
        const oldIntegrations = integrationsStorage.getIntegrations()
        const newIntegrations = await INTEGRATION_API.getIntegrations(teamId)
        const result = isSomethingChanged(oldIntegrations, newIntegrations.data)
        integrationsStorage.setIntegrations(newIntegrations.data)
        return result
    }
}
export default integrations
