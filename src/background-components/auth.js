import { USER_API } from '../__data__/api-map'

const authStorage = {
    getUser: () => JSON.parse(localStorage.getItem('user')),
    setUser: (user) => localStorage.setItem('user', JSON.stringify(user))
}

const auth = async () => {
    return new Promise((resolve) => {
        if (!authStorage.getUser()) {
            USER_API.info().then(({data}) => {
                authStorage.setUser(data)
                resolve(authStorage.getUser())
            })
        } else {
            resolve(authStorage.getUser())
        }
        
    })
}

export default auth
