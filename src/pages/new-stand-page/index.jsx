import React, { useCallback, useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { AlertDescription, AlertTitle, ButtonPrimary, Headline1, LabeledTextField, Link } from '@sbol/design-system'
import { ic24ChevronLeft } from '@sbol/design-system/core/icon/common'

import { Grid } from '../../components/UI'
import { AlertStyled, InputStyled } from '../new-task-page/new-task-page.style'
import { useUserInfo } from '../../utils/use-user-info'
import { useStandsPage } from '../stands-page/use-stands-page'
import {isFulfilled, isRejected} from "../../utils";

export const NewStandPage = () => {
    const { t } = useTranslation()
    const { segment } = useParams()
    const history = useHistory()
    const { team = {} } = useUserInfo()
    const { handleCreateStand } = useStandsPage()
    
    const [serverIp, setServerIp] = useState('')
    const [serverName, setServerName] = useState('')
    const [error, setError] = useState({})
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        setError({})
    }, [serverIp, serverName])
    
    const handlePreviousPage = useCallback(() => {
        history.goBack()
    }, [])

    const handleStandCreation = useCallback(async () => {
        if (!serverIp.length) {
            setError({
                serverIp: t('error.empty.server.ip')
            })
            return
        }
        if (!serverName.length) {
            setError({
                serverName: t('error.empty.server.name')
            })
            return
        }

        setLoading(true)
        const response = await handleCreateStand({
            name: serverName,
            ip: serverIp,
            type: segment,
            teamId: team.id
        })
        setLoading(false)

        if (isRejected(response)) {
            setError({
                creationError: response.payload
            })
            return
        }

        if (isFulfilled(response)) {
            history.goBack()
        }
    }, [segment, serverName, serverIp])

    return (
        <Grid direction="column">
            <Link
                title={t('link.back.title')}
                underlined={false}
                mode="breadcrumb"
                icon={ic24ChevronLeft}
                onClick={handlePreviousPage}
                verticalMargin="open"
                verticalMarginDirection="bottom"
            />
            <Headline1
                fontWeight="semibold"
                verticalMargin="nano"
            >
                {t('new.stand.page.header', { segment })}
            </Headline1>
            <Grid fullWidth direction="column" justify="flex-start" align="flex-start">
                <InputStyled
                    label={t('new.stand.page.ip.label')}
                    value={serverIp}
                    error={error.serverIp}
                    onChange={setServerIp}
                />
                <InputStyled
                    label={t('new.stand.page.name.label')}
                    value={serverName}
                    error={error.serverName}
                    onChange={setServerName}
                />
            </Grid>
            <Grid
                fullWidth
                direction="column"
                size="h1"
                verticalMargin="open"
                verticalMarginDirection="top"
            >
                {error.creationError && (
                    <AlertStyled mode="warning">
                        <AlertTitle>
                            {error.creationError.title}
                        </AlertTitle>
                        <AlertDescription>
                            {error.creationError.text}
                        </AlertDescription>
                    </AlertStyled>
                )}
                <ButtonPrimary
                    fullWidth
                    loading={loading}
                    verticalMargin="zero"
                    verticalMarginDirection="top"
                    title={t('tasks.new.branch.page.create')}
                    onClick={handleStandCreation}
                />
            </Grid>
        </Grid>
    )
}


export default NewStandPage
