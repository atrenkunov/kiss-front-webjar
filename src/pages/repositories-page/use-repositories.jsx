import { useDispatch, useSelector } from 'react-redux'
import { useCallback } from 'react'

import { actions, selectors } from '../../__data__'

export const useRepositories = () => {
    const dispatch = useDispatch()

    const teamInfo = useSelector(selectors.userSelectors.teamSelector)
    const list = useSelector(selectors.repositoriesSelectors.allReposSelector)
    const getAllRepositories = useCallback((teamId) => dispatch(actions.reposActions.getAllRepositories(teamId)), [])

    return {
        teamInfo,
        list,
        getAllRepositories
    }
}
