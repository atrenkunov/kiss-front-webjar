import React, { useEffect } from 'react'
import { Headline1, MarginWrapper } from '@sbol/design-system'
import { useTranslation } from 'react-i18next'

import { Grid } from '../../components/UI'
import { RepositoryCard } from '../../components/UI/repository-card'

import { useRepositories } from './use-repositories'

export const RepositoriesPage = () => {
    const { t } = useTranslation()
    const { teamInfo = {}, list = [], getAllRepositories } = useRepositories()

    useEffect(() => {
        if (teamInfo.id) {
            getAllRepositories(teamInfo.id)
        }
    }, [teamInfo.id])

    return (
        <Grid direction="column">
            <Headline1 indent="zero">
                {t('repositories.page.header', { teamId: teamInfo.name })}
            </Headline1>
            <MarginWrapper size="lg" verticalMargin="open">
                <Grid wrap>
                    {list.map((repo) => (
                        <MarginWrapper
                            key={repo.id}
                            size="lg"
                            horizontalMargin="inner"
                            horizontalMarginDirection="right"
                        >
                            <RepositoryCard item={repo} />
                        </MarginWrapper>

                    ))}
                </Grid>
            </MarginWrapper>
        </Grid>
    )
}

export default RepositoriesPage
