import styled from '@emotion/styled'
import { Banner } from '@sbol/design-system'


export const BannerStyled = styled(Banner)`
  cursor: pointer;
`
