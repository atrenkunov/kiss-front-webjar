import styled from '@emotion/styled'
import { Alert, LabeledTextField } from '@sbol/design-system'

export const InputStyled = styled(LabeledTextField)`
  min-width: 300px;
`

export const AlertStyled = styled(Alert)`
  width: 100%;
  margin-top: 0;
`
