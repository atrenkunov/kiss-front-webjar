import React, { useCallback, useState } from 'react'
import {
    AlertDescription,
    AlertTitle,
    ButtonPrimary,
    Headline1,
    Link,
    SegmentedGroup,
    SegmentedRadio
} from '@sbol/design-system'
import { ic24ChevronLeft } from '@sbol/design-system/core/icon/common'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router-dom'

import { Grid } from '../../components/UI'
import { TASK_TYPES } from '../../__data__/constants'
import { isFulfilled, isRejected } from '../../utils'

import { InputStyled, AlertStyled } from './new-task-page.style'
import { useNewTaskPage } from './use-new-task-page'
import { useUserInfo } from '../../utils/use-user-info'

const NewTaskPage = () => {
    const { t } = useTranslation()
    const history = useHistory()
    const { handleCreateTask } = useNewTaskPage()
    const { team } = useUserInfo()
    const [title, setTitle] = useState('')
    const [loading, setLoading] = useState(false)
    const [description, setDescription] = useState('')
    const [taskType, setTaskType] = useState(Object.keys(TASK_TYPES)[0])
    const [error, setError] = useState({})

    const handlePreviousPage = useCallback(() => {
        history.goBack()
    }, [])

    const handleTypeChange = useCallback((event) => {
        setTaskType(event.target.value)
        setError({})
    }, [])

    const handleTitleChange = useCallback((event) => {
        setTitle(event)
        setError({})
    }, [])

    const handleDescriptionChange = useCallback((event) => {
        setDescription(event)
        setError({})
    }, [])

    const handleTaskCreation = useCallback(async () => {
        if (!title) {
            setError({ title: t('error.empty.task.title') })
            return
        }

        if (!description) {
            setError({ description: t('error.empty.task.description') })
            return
        }

        setLoading(true)
        const response = await handleCreateTask({
            taskName: title,
            description,
            type: taskType,
            teamId: team.id
        })
        setLoading(false)

        if (isFulfilled(response)) {
            history.goBack()
        }

        if (isRejected(response)) {
            setError({
                creationError: response.payload
            })
        }
    }, [description, taskType, title, team.id])

    return (
        <Grid direction="column">
            <Link
                title={t('link.back.title')}
                underlined={false}
                mode="breadcrumb"
                icon={ic24ChevronLeft}
                onClick={handlePreviousPage}
                verticalMargin="open"
                verticalMarginDirection="bottom"
            />
            <Headline1
                fontWeight="semibold"
                verticalMargin="nano"
            >
                {t('new.task.page.header')}
            </Headline1>
            <Grid
                fullWidth
                direction="column"
            >
                <SegmentedGroup name="task-type">
                    {Object.entries(TASK_TYPES).map(([id, title]) => (
                        <SegmentedRadio
                            key={id}
                            value={id}
                            name={title}
                            onChange={handleTypeChange}
                            checked={id === taskType}
                        >
                            {title}
                        </SegmentedRadio>
                    ))}
                </SegmentedGroup>
                <InputStyled
                    label={t('new.task.page.title.label')}
                    error={error.title}
                    value={title}
                    onChange={handleTitleChange}
                />
                <InputStyled
                    error={error.description}
                    label={t('new.task.page.description.label')}
                    value={description}
                    onChange={handleDescriptionChange}
                />
                <Grid
                    fullWidth
                    direction="column"
                    size="h1"
                    verticalMargin="open"
                    verticalMarginDirection="top"
                >
                    {error.creationError && (
                        <AlertStyled mode="warning">
                            <AlertTitle>
                                {error.creationError.title}
                            </AlertTitle>
                            <AlertDescription>
                                {error.creationError.text}
                            </AlertDescription>
                        </AlertStyled>
                    )}
                    <ButtonPrimary
                        fullWidth
                        loading={loading}
                        verticalMargin="zero"
                        verticalMarginDirection="top"
                        title={t('tasks.new.branch.page.create')}
                        onClick={handleTaskCreation}
                    />
                </Grid>
            </Grid>
        </Grid>
    )
}

export default NewTaskPage
