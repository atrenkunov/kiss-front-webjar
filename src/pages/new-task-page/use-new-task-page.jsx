import { useDispatch, useSelector } from 'react-redux'
import { useCallback } from 'react'

import { actions } from '../../__data__'

export const useNewTaskPage = () => {
    const dispatch = useDispatch()
    
    const handleCreateTask = useCallback((data) => dispatch(actions.tasksActions.createTask(data)), [])
    
    return {
        handleCreateTask
    }
}
