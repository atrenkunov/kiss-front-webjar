import React, { useCallback } from 'react'
import { useHistory } from 'react-router-dom'
import { Headline1, Link } from '@sbol/design-system'
import { useTranslation } from 'react-i18next'
import { ic24ChevronLeft } from '@sbol/design-system/core/icon/common'

import { Grid } from '../../components/UI/grid'

export const NewRepositoryPage = () => {
    const history = useHistory()
    const { t } = useTranslation()

    const handlePreviousPage = useCallback(() => {
        history.goBack()
    }, [])

    return (
        <Grid direction="column">
            <Link
                title={t('link.back.title')}
                underlined={false}
                mode="breadcrumb"
                icon={ic24ChevronLeft}
                onClick={handlePreviousPage}
                verticalMargin="open"
                verticalMarginDirection="bottom"
            />
            <Headline1
                fontWeight="semibold"
                verticalMargin="nano"
            >
                {t('new.repository.page.header')}
            </Headline1>
        </Grid>
    )
}

export default NewRepositoryPage
