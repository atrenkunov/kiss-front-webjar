import { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { actions, selectors } from '../../__data__'

export const useRepositoryPage = () => {
    const dispatch = useDispatch()

    const currentRepository = useSelector(selectors.repositoriesSelectors.currentRepoSelector)
    const handleRepositoryRequest = useCallback((id) => dispatch(actions.reposActions.getRepository(id)), [dispatch])

    return {
        currentRepository,

        handleRepositoryRequest,
    }
}
