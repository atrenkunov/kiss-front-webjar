import React, { useEffect, useState, useCallback } from 'react'
import {
    ButtonPrimary,
    ButtonSecondary,
    Typography,
    Tabs,
    Tab,
    PaddingWrapper,
    SegmentedRadio,
    Body2
} from '@sbol/design-system'
import { useTranslation } from 'react-i18next'

import { INTEGRATION_API, TEAM_API } from '../../__data__/api-map'
import { Grid, Popup } from '../../components/UI'

import { StyledLogsContainer, StyledTextField, StyledContactBlock } from './integrations-page.style'

const TeamInfoRow = ({ title, value }) => (
    <div>
        <Typography
            as="span"
            size="md"
            verticalMargin="micro"
            gorizontalMargin="open"
            colorScheme="primary"
        >
            {`${title}: `}
        </Typography>
        <Typography
            as="span"
            size="md"
            verticalMargin="micro"
            colorScheme="secondary"
        >
            {value}
        </Typography>
    </div>
)

const ServerInfo = ({ server, rootTeam, title, onClose }) => {
    const { t } = useTranslation()
    const [loading, setLoading] = useState({ initial: true })
    const { id = '', destTeamId = '' } = server || {}
    const [teamInfo, setTeamInfo] = useState(null)
    const [serverLogs, setServerLogs] = useState([])
    const [showIssueReport, setShowIssueReport] = useState(false)

    const handleInitialFetch = useCallback(async () => {
        setLoading((prev) => ({ ...prev, initial: true }))
        const { data: logsData } = await INTEGRATION_API.getIntegrationLog(id)
        const { data: teamData } = await TEAM_API.getTeam(destTeamId)

        setServerLogs(logsData)
        setTeamInfo(teamData)

        setLoading((prev) => ({ ...prev, initial: false }))
    }, [])


    const handleIssueReport = useCallback(() => {
        setShowIssueReport(!showIssueReport)
    }, [showIssueReport])

    useEffect(() => {
        handleInitialFetch()
    }, [id])

    const handleDeleteIntegration = useCallback(() => {
        if (confirm('Вы уверены, что хотите удалить данную интеграцию?')) {
            INTEGRATION_API.deleteIntegration(id)
                .then(() => onClose())
        }
    }, [id, onClose])

    if (loading.initial) {
        return null
    }
    
    return (
        <Popup
            title={title || t('integration.modal.info.title', { server: server.integrationName })}
            onClose={onClose}
        >
            {
                server.integrationType && (
                    <Grid
                        size="md"
                        verticalMargin="inner"
                        verticalMarginDirection="bottom"
                        justify="flex-start"
                        align="center"
                    >
                        <Body2
                            verticalMargin="zero"
                            horizontalMargin="nano"
                            horizontalMarginDirection="right"
                        >
                            {t('integration.modal.info.type')}
                        </Body2>
                        <SegmentedRadio checked value={server.integrationType} name={server.integrationType}>
                            {server.integrationType}
                        </SegmentedRadio>
                    </Grid>
                )
            }
            <Tabs initialValue={t('integration.modal.tab.support')}>
                <Tab title={t('integration.modal.tab.support')}>
                    <PaddingWrapper size="lg" verticalPadding="inner" verticalPaddingDirection="top">
                        <StyledContactBlock size="lg" verticalMargin="micro" verticalMarginDirection="bottom">
                            <TeamInfoRow title="АС" value={teamInfo.name} />
                        </StyledContactBlock>
                        <StyledContactBlock size="lg" verticalMargin="micro" verticalMarginDirection="bottom">
                            <TeamInfoRow title="Владелец" value={teamInfo.owner} />
                            <TeamInfoRow title="тел." value={teamInfo.ownerContact} />
                        </StyledContactBlock>
                        <StyledContactBlock size="lg" verticalMargin="micro" verticalMarginDirection="bottom">
                            <TeamInfoRow title="Сопровождение" value={teamInfo.support} />
                            <TeamInfoRow title="тел." value={teamInfo.supportContact} />
                        </StyledContactBlock>
                    </PaddingWrapper>
                </Tab>
                {
                    Boolean(serverLogs.length) && (
                        <Tab title={t('integration.modal.tab.logs')}>
                            <PaddingWrapper size="lg" verticalPadding="inner" verticalPaddingDirection="both">
                                <StyledLogsContainer>
                                    {serverLogs.map((serverLog, idx) => (
                                        <div key={idx}>
                                            <div>{serverLog.timestamp}</div>
                                            <div>{serverLog.log}</div>
                                        </div>
                                    ))}
                                </StyledLogsContainer>
                            </PaddingWrapper>
                        </Tab>
                    )
                }
            </Tabs>
            {
                showIssueReport
                    ? <>
                        <StyledTextField />
                        <ButtonPrimary  title={t('integration.modal.button.send')} onClick={handleIssueReport} />
                        <ButtonSecondary  title={t('integration.modal.button.cancel')} onClick={handleIssueReport} />
                    </>
                    : <>
                        {
                            !rootTeam && (
                                <>
                                    <ButtonSecondary title={t('integration.modal.button.delete')} onClick={handleDeleteIntegration} />
                                    <ButtonSecondary
                                        title={t('integration.modal.button.report')}
                                        onClick={handleIssueReport}
                                        horizontalMargin="open"
                                        horizontalMarginDirection="right"
                                    />
                                </>
                            )
                        }
                        <ButtonPrimary
                            horizontalMargin={rootTeam ? 'zero' : 'open'}
                            horizontalMarginDirection={rootTeam ? 'both' : 'left'}
                            title={t('integration.modal.button.close')}
                            onClick={onClose}
                        />
                    </>
            }
        </Popup>
    )

}

export default ServerInfo
