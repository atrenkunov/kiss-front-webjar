import { useDispatch, useSelector } from 'react-redux'
import { useCallback } from 'react'

import { actions, selectors } from '../../__data__'

export const useIntegrationPage = () => {
    const dispatch = useDispatch()
    
    const teams = useSelector(selectors.integrationSelectors.otherTeamsSelector)
    const integrationTypes = useSelector(selectors.integrationSelectors.typesSelector)

    const fetchTeams = useCallback(() => dispatch(actions.integrationActions.getTeamsList()), [dispatch])
    const fetchServiceStands = useCallback((teamId) => dispatch(actions.nexusActions.getStandsList({ standType: 'IFT', teamId })), [])
    const fetchIntegrationTypes = useCallback(() => dispatch(actions.integrationActions.getIntegrationTypes()), [dispatch])

    return {
        teams,
        integrationTypes,
        
        fetchTeams,
        fetchServiceStands,
        fetchIntegrationTypes
    }
}
