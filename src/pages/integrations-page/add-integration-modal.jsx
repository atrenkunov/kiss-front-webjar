import React, { useCallback, useEffect, useState } from 'react'
import {
    ButtonPrimary,
    ButtonSecondary,
    LabeledValueSelect,
    ValueOption,
    LabeledTextField,
    SegmentedGroup, SegmentedRadio
} from '@sbol/design-system'
import { useTranslation } from 'react-i18next'

import { Popup } from '../../components/UI'
import { INTEGRATION_API } from '../../__data__/api-map'
import { useUserInfo } from '../../utils/use-user-info'

import { useIntegrationPage } from './use-integration-page'

const AddIntegrationModal = ({ closeModal }) => {
    const { t } = useTranslation()
    const { team = {} } = useUserInfo()
    const { teams = [], integrationTypes = [], fetchTeams, fetchServiceStands, fetchIntegrationTypes } = useIntegrationPage()
    const [selectedTeam, setSelectedTeam] = useState(null)
    const [selectedType, setSelectedType] = useState(integrationTypes[0])

    const [serverName, setServerName] = useState('')

    const handleInitialFetch = useCallback(async () => {
        await fetchTeams()
        const { payload } = await fetchIntegrationTypes()
        setSelectedType(payload[0])
    }, [])

    useEffect(() => {
        handleInitialFetch()
    }, [])

    const handleAdd = useCallback(async () => {
        await INTEGRATION_API.addIntegration({
            sourceTeamId: team.id,
            integrationType: selectedType,
            integrationName: `[${teams.find((team) => team.id === selectedTeam).name}] ${serverName}`.trim(),
            destTeamId: selectedTeam
        })

        closeModal()
    }, [serverName, selectedTeam, team.id, selectedType])

    const handleTeamSelect = useCallback(async (id) => {
        setSelectedTeam(id)
    }, [])

    const handleTypeSelect = useCallback((event) => {
        setSelectedType(event.target.value)
    }, [])

    return (
        <Popup onClose={closeModal} title={t('integrations.button.add')}>
            <SegmentedGroup name="integration type">
                {integrationTypes.map((type) => (
                    <SegmentedRadio
                        value={type}
                        name={type}
                        key={type}
                        checked={selectedType === type}
                        onChange={handleTypeSelect}
                    >
                        {type}
                    </SegmentedRadio>
                ))}
            </SegmentedGroup>
            <LabeledValueSelect
                label={t('integration.creation.team.label')}
                description={t('integration.creation.team.description')}
                value={selectedTeam}
                onChange={handleTeamSelect}
            >
                {teams.map((team) => (
                    <ValueOption
                        key={team.id}
                        title={team.name}
                        value={team.id}
                    />
                ))}
            </LabeledValueSelect>
            <LabeledTextField
                value={serverName}
                onChange={setServerName}
                label={t('integration.creation.service.name.label')}
                description={t('integration.creation.service.name.description')}
            />
            <div>
                <ButtonPrimary onClick={handleAdd} title={t('integration.modal.button.add')} />
                <ButtonSecondary onClick={closeModal} title={t('integration.modal.button.cancel')} />
            </div>
        </Popup>
    )
}

export default AddIntegrationModal
