import styled from '@emotion/styled'
import { MarginWrapper, MultilineTextField } from '@sbol/design-system'
import { css } from '@emotion/react'

const getXcoordinateOnCircle = (radius, angle) => (radius * Math.cos((angle + (-90)) * (Math.PI / 180)))
const getYcoordinateOnCircle = (radius, angle) => (radius * Math.sin((angle + (-90)) * (Math.PI / 180)))

export const IntegrationBox = styled.div(({ theme, colorScheme, radius, elementsCount, elementNumber }) => css`
  position: absolute;
  cursor: pointer;
  white-space: nowrap;
  padding: 15px;
  border-radius: 50px;
  left: ${getXcoordinateOnCircle(radius, 360 / elementsCount * elementNumber)}px;
  top: ${getYcoordinateOnCircle(radius, 360 / elementsCount * elementNumber)}px;
  border: 1px solid ${theme[colorScheme]};
`)

export const StyledCenterElement = styled.div`
  white-space: nowrap;
  position: relative;
  left: 200px;
  top: 250px;
`

export const TeamBox = styled.div(({ theme }) => css`
  padding: 15px;
  border-radius: 50px;
  border: 1px solid ${theme.primary};
  user-select: none;
  cursor: pointer;
`)


export const StyledLogsContainer = styled.div`
  max-width: 100%;
  max-height: 600px;
  overflow-y: auto;
`

export const StyledTextField = styled(MultilineTextField)`
  margin-top: 16px;
  min-height: 56px;
`

export const StyledContactBlock = styled(MarginWrapper)`
  display: flex;
  flex-direction: column;
  width: 228px;
`
