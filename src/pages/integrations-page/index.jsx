import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useSelector } from 'react-redux'
import { ButtonSecondary, Headline1, Headline3 } from '@sbol/design-system'
import { useTranslation } from 'react-i18next'

import { selectors } from '../../__data__'
import { Grid } from '../../components/UI/grid'
import { INTEGRATION_API } from '../../__data__/api-map'

import AddIntegrationModal from './add-integration-modal'
import IntegratedServer from './integrated-server'
import {
    StyledCenterElement,
    TeamBox,
} from './integrations-page.style'
import ServerInfo from './server-info-modal'

const IntegrationsPage = () => {
    const [servers, setServers] = useState([])

    const [isModalOpened, setIsModalOpened] = useState(false)
    const [showTeamInfo, setShowTeamInfo] = useState(false)

    const { t } = useTranslation()
    const currentTeamRef = useRef(null)
    const team = useSelector(selectors.userSelectors.teamSelector)
  
    const updateServersState = useCallback(() => {
        INTEGRATION_API.getIntegrations(team.id)
            .then(({ data }) => setServers(data))
    }, [team.id])

    const handleToggleIntegrationModal = useCallback(() => {
        setIsModalOpened(!isModalOpened)
    }, [isModalOpened])

    const handleTeamInfo = useCallback(() => {
        setShowTeamInfo(!showTeamInfo)
    }, [showTeamInfo])

    useEffect(() => {
        if (team.id) {
            updateServersState()
            const updateInterval = setInterval(updateServersState, 1000)
            return () => {
                clearInterval(updateInterval)
            }
        }
    }, [team.id])

    return (
        <Grid direction="column">
            <Headline1
                fontWeight="semibold"
                indent="zero"
            >
                {t('integrations.page.header')}
            </Headline1>
            <ButtonSecondary
                verticalMargin="open"
                verticalMarginDirection="top"
                horizontalMargin="zero"
                title={t('integrations.button.add')}
                onClick={handleToggleIntegrationModal}
            />
            <Grid
                fullWidth
                size="lg"
                verticalMargin="open"
                verticalMarginDirection="top"
                align="space-between"
            >
                <StyledCenterElement>
                    <TeamBox ref={currentTeamRef} onClick={handleTeamInfo}>
                        <Headline3
                            verticalMargin="zero"
                            horizontalMargin="zero"
                            indent="zero"
                            horizontalMarginDirection="left"
                        >
                            {team.name}
                        </Headline3>
                    </TeamBox>
                    { servers.map((element, index) => (
                        <IntegratedServer
                            key={element.id}
                            rootTeamRef={currentTeamRef}
                            server={element}
                            numberInList={index}
                            serversCount={servers.length}
                        />
                    ))}
                </StyledCenterElement>
            </Grid>
            { isModalOpened && <AddIntegrationModal closeModal={handleToggleIntegrationModal} /> }
            { showTeamInfo && (
                <ServerInfo
                    rootTeam
                    title={t('integration.modal.info.root', { name: team.name })}
                    server={{ id: team.id, destTeamId: team.id }}
                    onClose={handleTeamInfo}
                />
            )}
        </Grid>
    )
}

export default IntegrationsPage
