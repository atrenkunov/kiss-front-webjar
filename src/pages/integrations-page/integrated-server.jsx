import React, { useRef, useCallback, useState } from 'react'
import { Body2, IconLoader, MarginWrapper } from '@sbol/design-system'
import Xarrow from 'react-xarrows'

import { GLOBAL_THEME } from '../../components/UI/theme-wrapper/theme'
import { Grid } from '../../components/UI'

import { IntegrationBox } from './integrations-page.style'
import ServerInfo from './server-info-modal'

const IntegratedServer = ({ rootTeamRef, server, numberInList, serversCount }) => {
    const [showInfo, setShowInfo] = useState(false)
    const serverRef = useRef(null)

    const handleDetailedInfo = useCallback(async () => {
        setShowInfo(!showInfo)
    }, [showInfo])

    return (
        <>
            <IntegrationBox
                ref={serverRef}
                radius={200}
                elementsCount={serversCount}
                elementNumber={numberInList}
                onClick={handleDetailedInfo}
                colorScheme={server.status === 'OK' ? 'success' : 'warningTransparent40'}
            >
                <Grid>
                    <IconLoader
                        name={`icon:sberboard/common/${server.status === 'OK' ? 'ic24CheckmarkCircle' : 'ic24Bolt'}`}
                        colorScheme={server.status === 'OK' ? 'success' : 'warningTransparent40'}
                    />
                    <MarginWrapper size="sm" horizontalMargin="nano" horizontalMarginDirection="left">
                        <Body2
                            verticalMargin="zero"
                            fontWeight="semibold"
                            colorScheme={server.status === 'OK' ? 'success' : 'warning'}
                        >
                            { server.integrationName }
                        </Body2>
                    </MarginWrapper>
                </Grid>

            </IntegrationBox>
            <Xarrow
                dashness={server.status !== 'OK'}
                showHead
                animateDrawing
                headShape="arrow"
                strokeWidth={2}
                path="smooth"
                color={server.status === 'OK' ? GLOBAL_THEME.LIGHT_THEME.success : GLOBAL_THEME.LIGHT_THEME.warningTransparent40}
                start={rootTeamRef}
                end={serverRef}
            />
            { showInfo && <ServerInfo server={server} onClose={handleDetailedInfo} /> }
        </>
    )
}
export default IntegratedServer
