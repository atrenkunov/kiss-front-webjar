import { useDispatch, useSelector } from 'react-redux'
import { useCallback } from 'react'

import { actions, selectors } from '../../__data__'

export const useNewBranchPage = () => {
    const dispatch = useDispatch()

    const repositories = useSelector(selectors.repositoriesSelectors.allReposSelector)
    const branches = useSelector(selectors.repositoriesSelectors.currentRepoBranchesSelector)

    const fetchBranches = useCallback((repoId) => dispatch(actions.reposActions.getRepository(repoId)), [dispatch])
    const createBranch = useCallback((repoId, data) => dispatch(actions.reposActions.createBranch({ repoId, data })), [dispatch])
    
    return {
        repositories,
        branches,

        fetchBranches,
        createBranch
    }
}
