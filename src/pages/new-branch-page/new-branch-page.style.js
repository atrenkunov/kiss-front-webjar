import styled from "@emotion/styled";
import {Alert} from "@sbol/design-system";


export const FormContainer = styled.div`
  min-height: 50vh;
`

export const AlertStyled = styled(Alert)`
  width: 100%;
  margin-top: 0;
`