import React, { useCallback, useState } from 'react'
import { useParams, useHistory } from 'react-router-dom'
import {
    ButtonPrimary,
    Headline1,
    LabeledValueSelect,
    Link,
    SegmentedGroup,
    SegmentedRadio,
    ValueOption,
    Loader,
    AlertDescription,
    AlertTitle
} from '@sbol/design-system'
import { ic24ChevronLeft } from '@sbol/design-system/core/icon/common'
import { useTranslation } from 'react-i18next'
import { disableHandler } from '@sbol/design-system/core/utils/handlers'

import { Grid } from '../../components/UI/grid'
import { BRANCH_TYPES } from '../../__data__/constants'
import { isFulfilled, isRejected } from '../../utils'

import { useNewBranchPage } from './use-new-branch-page'
import { FormContainer, AlertStyled } from './new-branch-page.style'

export const NewBranchPage = () => {
    const { t } = useTranslation()
    const { taskId } = useParams()
    const history = useHistory()

    const [selectedRepo, setSelectedRepo] = useState('')
    const [selectedBranch, setSelectedBranch] = useState('')
    const [newBranchType, setNewBranchType] = useState(BRANCH_TYPES.FEATURE)

    const [creatingBranch, setCreatingBranch] = useState(false)
    const [branchesLoading, setBranchesLoading] = useState(false)
    const [error, setError] = useState({})

    const { repositories = [], branches = [], fetchBranches, createBranch } = useNewBranchPage()

    const handlePreviousPage = useCallback(() => {
        history.goBack()
    }, [])
    
    const handleRepositorySelect = useCallback(async (event) => {
        setBranchesLoading(true)
        await fetchBranches(event)
        setBranchesLoading(false)
        setSelectedRepo(event)
        setSelectedBranch('')
        setError({})
    }, [])

    const handleBranchSelect = useCallback((event) => {
        setSelectedBranch(event)
        setError({})
    }, [])

    const handleBranchTypeSelect = useCallback((event) => {
        setNewBranchType(event.target.value)
        setError({})
    }, [])

    const handleBranchCreate = useCallback(async () => {
        if (!selectedRepo) {
            setError({
                repo: t('error.empty.repo')
            })
            return
        }

        if (!selectedBranch) {
            setError({
                branch: t('error.empty.branch')
            })
            return
        }

        setCreatingBranch(true)

        const response = await createBranch(selectedRepo, {
            id: `${newBranchType}/${taskId}`,
            repositoryId: selectedRepo,
            sourceId: selectedBranch
        })
        setCreatingBranch(false)

        if (isFulfilled(response)) {
            history.goBack()
        }

        if (isRejected(response)) {
            setError({
                creationError: response.payload
            })
        }
    }, [selectedRepo, selectedBranch, taskId, newBranchType])

    return (
        <Grid direction="column">
            <Link
                title={t('link.back.title')}
                underlined={false}
                mode="breadcrumb"
                icon={ic24ChevronLeft}
                onClick={handlePreviousPage}
                verticalMargin="open"
                verticalMarginDirection="bottom"
            />
            <Headline1
                fontWeight="semibold"
                verticalMargin="nano"
            >
                {t('tasks.new.branch.page', { taskId })}
            </Headline1>
            <Grid fullWidth direction="column" justify="flex-start" align="flex-start">
                <FormContainer>
                    <SegmentedGroup size="md" name="Тип новой ветки">
                        {Object.entries(BRANCH_TYPES).map(([key, value]) => (
                            <SegmentedRadio
                                key={key}
                                value={value}
                                name={value}
                                checked={value === newBranchType}
                                onChange={handleBranchTypeSelect}
                            >
                                {value}
                            </SegmentedRadio>
                        ))}
                    </SegmentedGroup>
                    <LabeledValueSelect
                        label={t('tasks.new.branch.repo.select.label')}
                        description={t('tasks.new.branch.repo.select.description')}
                        value={selectedRepo}
                        error={error.repo}
                        onChange={handleRepositorySelect}
                    >
                        {repositories.map((repo) => (
                            <ValueOption
                                key={repo.id}
                                title={repo.name}
                                value={repo.id}
                            />
                        ))}
                    </LabeledValueSelect>
                    {branchesLoading ? <Loader /> : (
                        <>
                            {
                                Boolean(branches.length) && selectedRepo &&  (
                                    <LabeledValueSelect
                                        label={t('tasks.new.branch.branch.select.label')}
                                        description={t('tasks.new.branch.branch.select.description')}
                                        value={selectedBranch}
                                        error={error.branch}
                                        onChange={handleBranchSelect}
                                    >
                                        {branches.map((branch) => (
                                            <ValueOption
                                                key={branch.id}
                                                title={branch.name}
                                                value={branch.id}
                                            />
                                        ))}
                                    </LabeledValueSelect>
                                )
                            }
                        </>
                    )}
                </FormContainer>
                <Grid
                    fullWidth
                    direction="column"
                >
                    {selectedRepo && !Boolean(branches.length) && (
                        <AlertStyled mode="info">
                            <AlertDescription>{t('error.parent.branch.empty.text')}</AlertDescription>
                        </AlertStyled>
                    )}
                    {error.creationError && (
                        <AlertStyled mode="warning">
                            <AlertTitle>
                                {error.creationError.title}
                            </AlertTitle>
                            <AlertDescription>
                                {error.creationError.text}
                            </AlertDescription>
                        </AlertStyled>
                    )}
                    <ButtonPrimary
                        fullWidth
                        loading={creatingBranch}
                        verticalMargin="zero"
                        verticalMarginDirection="top"
                        title={t('tasks.new.branch.page.create')}
                        onClick={disableHandler(handleBranchCreate, error.creationError)}
                    />
                </Grid>
            </Grid>
        </Grid>
    )
}

export default NewBranchPage
