import React, { useCallback } from 'react'
import { Headline1, Link } from '@sbol/design-system'
import { useTranslation } from 'react-i18next'
import { ic24ChevronLeft } from '@sbol/design-system/core/icon/common'
import { useParams, useHistory } from 'react-router-dom'

import { Grid } from '../../components/UI'
import BuildProcess from '../../components/build-process'

const BuildPage = () => {
    const history = useHistory()
    const { buildId } = useParams()
    const { t } = useTranslation()

    const handlePreviousPage = useCallback(() => {
        history.goBack()
    }, [])

    return (
        <Grid direction="column">
            <Link
                title={t('link.back.title')}
                underlined={false}
                mode="breadcrumb"
                icon={ic24ChevronLeft}
                onClick={handlePreviousPage}
                verticalMargin="open"
                verticalMarginDirection="bottom"
            />
            <Headline1
                fontWeight="semibold"
                verticalMargin="nano"
            >
                {t('build.page.header')}
            </Headline1>
            <BuildProcess branchBuildId={buildId} />
        </Grid>
    )
}

export default BuildPage
