import React, { useCallback, useEffect, useMemo, useState } from 'react'
import {ButtonPrimary, ButtonSecondary, Headline1, PageLoader, TableCell, TableHead} from '@sbol/design-system'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router-dom'

import { Grid } from '../../components/UI/grid'
import { useUserInfo } from '../../utils/use-user-info'
import { APP_ROUTES, CREATE_STAND_LINK, STANDS_TYPE } from '../../__data__/constants'
import { StandsSelector } from '../../components/stand-selector'
import { NotificationScreen } from '../../components/UI/notification-screen'
import { StandRow } from '../../components/stand-row'

import { useStandsPage } from './use-stands-page'
import { TableStyled } from './stands-page.style'

const TABLE_HEADERS = [
    {
        key: 'ip',
        title: 'IP',
    },
    {
        key: 'name',
        title: 'Название задачи'
    },
    {
        key: 'branchId',
        title: 'Ветка'
    },
    {
        key: 'moduleVersion',
        title: 'Версия'
    },
    {
        key: 'owner',
        title: 'Кто установил'
    },
    {
        key: 'description',
        title: 'Описание'
    },
    {
        key: 'status',
        title: 'Статус',
        align: 'right'
    }
]

export const StandsPage = () => {
    const { t } = useTranslation()
    const history = useHistory()

    const [selectedSegment, setSelectedSegment] = useState(STANDS_TYPE.DEV)
    const [loading, setLoading] = useState(true)

    const { stands = [], installationStages, fetchStands, fetchArtifacts, fetchStages } = useStandsPage()
    const { team } = useUserInfo()

    const standsItems = useMemo(() => Object.values(STANDS_TYPE).map((stand) => ({ value: stand, title: stand })), [])
    const hasRunningInstallations = useMemo(() =>
        stands.some((stand) =>
            stand.installationProgress && !['N/A', 'COMPLETED'].includes(stand.installationProgress.state
            )), [stands])

    const handleSegmentChange = useCallback((event) => setSelectedSegment(event.target.value), [])

    const handleStandsFetch = useCallback(async () => {
        setLoading(true)
        await fetchStands(selectedSegment, team.id)
        await fetchArtifacts(team.id)
        setLoading(false)
    }, [team.id, selectedSegment])
    
    const handleRedirect = useCallback(() => {
        history.push(CREATE_STAND_LINK(selectedSegment))
    }, [selectedSegment])

    useEffect(() => {
        if (team.id) {
            handleStandsFetch()
        }
    }, [team.id, selectedSegment])

    useEffect(() => {
        if (!installationStages.length) {
            fetchStages()
        }
    }, [])
    
    useEffect(() => {
        let updateInterval
        if (hasRunningInstallations) {
            handleStandsFetch()
            updateInterval = setInterval(() => handleStandsFetch(), 2000)
        }

        return () => {
            clearInterval(updateInterval)
        }
    }, [hasRunningInstallations])

    return (
        <Grid direction="column" fullWidth>
            <Headline1
                fontWeight="semibold"
                indent="zero"
            >
                {t('stands.page.header')}
            </Headline1>
            <ButtonSecondary
                verticalMargin="open"
                verticalMarginDirection="top"
                horizontalMargin="zero"
                title={t('stands.page.new.stand.button.header')}
                onClick={handleRedirect}
            />
            <Grid fullWidth direction="column" size="lg" verticalMargin="open" verticalMarginDirection="top">
                <StandsSelector
                    items={standsItems}
                    value={selectedSegment}
                    onChange={handleSegmentChange}
                />
                {
                    loading && !stands.length ? <PageLoader /> : (
                        <Grid
                            fullWidth
                            direction="column"
                            verticalMargin="inner"
                            verticalMarginDireciton="top"
                        >
                            {
                                Boolean(stands.length) ? (
                                    <TableStyled size="md">
                                        <TableHead>
                                            {TABLE_HEADERS.map((header) => (
                                                <TableCell key={header.key}>{header.title}</TableCell>
                                            ))}
                                        </TableHead>
                                        {stands.map((stand) => <StandRow key={stand.id} stand={stand} />)}
                                    </TableStyled>
                                ) : (
                                    <NotificationScreen
                                        title={t('notification.empty.title.stands')}
                                        description={t('notification.empty.description.stands')}
                                    >
                                        <ButtonPrimary
                                            title={t('stands.page.new.stand.button')}
                                            onClick={handleRedirect}
                                        />
                                    </NotificationScreen>
                                )
                            }
                        </Grid>
                    )
                }
            </Grid>
        </Grid>
    )
}

export default StandsPage
