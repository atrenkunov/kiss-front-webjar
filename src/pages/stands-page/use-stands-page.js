import { useDispatch, useSelector } from 'react-redux'
import { useCallback } from 'react'

import { actions, selectors } from '../../__data__'

export const useStandsPage = () => {
    const dispatch = useDispatch()

    const stands = useSelector(selectors.nexusSelectors.standsSelector)
    const installationStages = useSelector(selectors.executorSelectors.installationStagesSelector)

    const fetchStands = useCallback((standType, teamId) => dispatch(actions.nexusActions.getStandsList({standType, teamId})), [dispatch])
    const fetchArtifacts = useCallback((teamId) => dispatch(actions.nexusActions.getAllArtifacts(teamId)), [dispatch])
    const fetchStages = useCallback(() => dispatch(actions.executorActions.getInstallationStages()), [dispatch])
    const refreshInstallInfo = useCallback((installId, standIp) => dispatch(actions.executorActions.getInstallStatus({installId, standIp})), [dispatch])
    const handleCreateStand = useCallback((data) => dispatch(actions.nexusActions.createStand(data)), [dispatch])

    return {
        stands,
        installationStages,

        fetchStands,
        fetchArtifacts,
        fetchStages,
        refreshInstallInfo,
        handleCreateStand
    }
}
