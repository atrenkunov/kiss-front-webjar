import React, { useCallback, useEffect, useState } from 'react'
import { ButtonPrimary, ButtonSecondary, Headline1, PageLoader, TableCell, TableHead } from '@sbol/design-system'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router-dom'

import { Grid } from '../../components/UI/grid'
import { useUserInfo } from '../../utils/use-user-info'
import { TableStyled } from '../stands-page/stands-page.style'
import { NotificationScreen } from '../../components/UI/notification-screen'
import { APP_ROUTES } from '../../__data__/constants'

import { usePlannerPage } from './use-planner-page'
import { PlannerRow } from './planner-row'


const TABLE_HEADERS = [
    {
        key: 'task',
        title: 'Задача'
    },
    {
        key: 'branch',
        title: 'Ветка'
    },
    {
        key: 'description',
        title: 'Описание'
    },
    {
        key: 'status',
        title: 'Статус задачи'
    },
    {
        key: 'inSprint',
        title: 'Спринт'
    }
]

export const PlannerPage = () => {
    const history = useHistory()
    const { t } = useTranslation()
    const { allTasks = [], fetchAllTasks } = usePlannerPage()
    const { team } = useUserInfo()
    const [loading, setLoading] = useState(true)

    const handleTasksFetch = useCallback(async () => {
        setLoading(true)
        await fetchAllTasks()
        setLoading(false)
    }, [team.id])

    useEffect(() => {
        handleTasksFetch()
    }, [])

    const handleRedirect = useCallback(() => {
        history.push(APP_ROUTES.TASKS_NEW)
    }, [])

    return (
        <Grid direction="column">
            <Headline1
                fontWeight="semibold"
                indent="zero"
            >
                {t('planner.page.header')}
            </Headline1>
            <ButtonSecondary
                title={t('planner.page.create.task.button.title')}
                verticalMargin="open"
                verticalMarginDirection="top"
                horizontalMargin="zero"
                onClick={handleRedirect}
            />
            <Grid
                fullWidth
                direction="column"
                size="lg"
                verticalMargin="open"
                verticalMarginDirection="top"
            >
                {loading ? <PageLoader /> : (
                    <Grid fullWidth direction="column" justify="flex-start" align="flex-start">
                        {Boolean(allTasks.length) ? (
                            <TableStyled size="md">
                                <TableHead>
                                    {TABLE_HEADERS.map((header) => (
                                        <TableCell key={header.key}>{header.title}</TableCell>
                                    ))}
                                </TableHead>
                                {allTasks.map((task) => <PlannerRow task={task} key={task.taskId} />)}
                            </TableStyled>
                        ) : (
                            <NotificationScreen
                                title={t('notifications.empty.title.tasks')}
                                description={t('notifications.empty.description.tasks')}
                            >
                                <ButtonPrimary
                                    title={t('planner.page.create.task.button.title')}
                                    horizontalMargin="zero"
                                    onClick={handleRedirect}
                                />
                            </NotificationScreen>
                        )}
                    </Grid>
                )}
            </Grid>
        </Grid>
    )
}

export default PlannerPage
