import { useDispatch, useSelector } from 'react-redux'
import { useCallback } from 'react'

import { actions, selectors } from '../../__data__'

export const usePlannerPage = () => {
    const dispatch = useDispatch()

    const allTasks = useSelector(selectors.tasksSelectors.allTasksSelector)

    const fetchAllTasks = useCallback(() => dispatch(actions.tasksActions.getAllTasks()), [dispatch])
    const handleSelectForSprint = useCallback((taskId) => dispatch(actions.tasksActions.selectTask(taskId)), [dispatch])

    return {
        allTasks,

        fetchAllTasks,
        handleSelectForSprint
    }
}
