import React, { useCallback } from 'react'
import { useTranslation } from 'react-i18next'
import { Body2, Checkbox, ExternalLink, Link, TableCell, TableRow } from '@sbol/design-system'
import { useHistory } from 'react-router-dom'

import { Grid } from '../../components/UI'
import { HTML_ENTITIES, REPOSITORY_LINK, VIEWER_LINK } from '../../__data__/constants'
import { beautify } from '../../utils'

import { usePlannerPage } from './use-planner-page'

export const PlannerRow = ({ task = {} }) => {
    const { t } = useTranslation()
    const history = useHistory()

    const { branch = {} } = task

    const { handleSelectForSprint } = usePlannerPage()


    const handleTaskSelect = useCallback((selected) => {
        handleSelectForSprint({ taskId: task.taskId, selected })
    }, [task.taskId])

    const handleRedirect = useCallback(() => {
        history.push(REPOSITORY_LINK(branch?.repositoryId))
    }, [branch?.repositoryId])

    const handleViewerRedirect = useCallback(() => {
        history.push(VIEWER_LINK(task.taskId))
    }, [task.taskId])

    const TaskIdWrapper = task.jiraUrl ? ExternalLink : Body2
    return (
        <TableRow key={task.taskId}>
            <TableCell>
                <Grid direction="column">
                    <TaskIdWrapper
                        verticalMargin="zero"
                        fontWeight="semibold"
                        href={task.jiraUrl}
                        title={task.taskId}
                    >
                        {task.taskId}
                    </TaskIdWrapper>
                    <Body2 verticalMargin="zero" colorScheme="secondary">
                        {task.taskName}
                    </Body2>
                </Grid>
            </TableCell>
            <TableCell>
                {
                    branch?.id ? (
                        <Grid direction="column">
                            <Link
                                onClick={handleRedirect}
                                title={branch?.repositoryId}
                            />
                            <Body2 verticalMargin="zero">
                                {branch?.id}
                            </Body2>
                        </Grid>
                    ) : beautify(HTML_ENTITIES.EM_DASH)
                }
            </TableCell>
            <TableCell>
                <Grid direction="column">
                    <Body2
                        verticalMargin="zero"
                        horizontalMargin="inner"
                        horizontalMarginDirection="right"
                    >
                        {task.description}
                    </Body2>
                    <Link
                        title={t('planner.page.viewer.link')}
                        fontWeight="semibold"
                        onClick={handleViewerRedirect}
                    />
                </Grid>
            </TableCell>
            <TableCell>
                <Body2 verticalMargin="zero">
                    {task.taskStatus}
                </Body2>
            </TableCell>
            <TableCell>
                <Checkbox
                    checked={task.selected}
                    onChange={handleTaskSelect}
                >
                    {t(`planner.page.${task.selected ? 'added' : 'add'}.to.sprint`)}
                </Checkbox>
            </TableCell>
        </TableRow>
    )
}
