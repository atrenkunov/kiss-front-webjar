import React, { useEffect, useRef, useState } from 'react'
import {Headline1, Link, Table, TableRow, TableCell, TableHead, PageLoader, Body2} from '@sbol/design-system'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'

import BuildArtifact from '../../components/build-artifact'
import { Grid, NotificationScreen } from '../../components/UI'
import { selectors } from '../../__data__/'
import { EXECUTOR_API } from '../../__data__/api-map'
import { APP_ROUTES, EXECUTOR_STAGES_TYPES } from '../../__data__/constants'

const BuildsPage = () => {
    const { t } = useTranslation()
    const timer = useRef(null)

    const [branchBuildStages, setBranchBuildStages] = useState({})
    const [currentStatus, setCurrentStatus] = useState(null)
    const teamInfo = useSelector(selectors.userSelectors.teamSelector)
 
    const receiveStatuses = () => {
        if (teamInfo.id) {
            EXECUTOR_API.getActiveBuildsByTeamId(teamInfo.id).then(({ data }) => {
                // todo: что будет, если между стартом запроса и этим моментом произойдет unmount?
                setCurrentStatus(data)
                if (data.find((build) => build.state !== 'COMPLETED')) {
                    timer.current = setTimeout(receiveStatuses, 2000)
                }
            })
        }
    }

    useEffect(() => {
        if (teamInfo.id) {
            EXECUTOR_API.getStages(EXECUTOR_STAGES_TYPES.BUILD).then(({ data }) => setBranchBuildStages(
                data.reduce((acc, cur) => {
                    acc[cur.id] = cur.name
                    return acc
                }, {})
            ))
      
            receiveStatuses()
        }
        return () => {
            clearTimeout(timer.current)
        }
    }, [teamInfo.id])

    if (!currentStatus) {
        return <PageLoader />
    }

    return (
        <Grid direction="column" fullWidth>
            <Headline1
                fontWeight="semibold"
                verticalMargin="nano"
            >
                {t('builds.page.header')}
            </Headline1>
            {
                Boolean(currentStatus.length) ? (
                    <Table>
                        <TableHead>
                            <TableCell>ID</TableCell>
                            <TableCell>Репозиторий</TableCell>
                            <TableCell>Ветка</TableCell>
                            <TableCell>Версия модуля</TableCell>
                            { currentStatus[0].stages.map((oneStage) => (
                                <TableCell key={oneStage.id}>
                                    { branchBuildStages[oneStage.id] }
                                </TableCell>
                            ))}
                            <TableCell>Артефакт</TableCell>
                        </TableHead>
                        { currentStatus && currentStatus.map((build) => (
                            <TableRow key={build.id}>
                                <TableCell>{ build.id }</TableCell>
                                <TableCell>{ build.repositoryId }</TableCell>
                                <TableCell>{ build.branchId }</TableCell>
                                <TableCell>
                                    <Body2 verticalMargin="zero" horizontalMargin="open" horizontalMarginDirection="left">
                                        { build.moduleVersion }
                                    </Body2>
                                </TableCell>
                                { build.stages.map((oneStage) => (
                                    <TableCell key={oneStage.id}>
                                        {oneStage.progress}%
                                    </TableCell>
                                ))}
                                <TableCell>
                                    { build.state === 'COMPLETED' ? <BuildArtifact buildId={build.id} /> : 'n/a' }
                                </TableCell>
                            </TableRow>
                        )) }
                        { !currentStatus && 'Загрузка...' }
                    </Table>
                ) : (
                    <NotificationScreen
                        title={t('notification.empty.title.builds')}
                        description={t('notification.empty.description.builds')}
                    >
                        <Link
                            fontWeight="semibold"
                            underlined={false}
                            title={t('notification.empty.link.builds')}
                            colorScheme="success"
                            href={APP_ROUTES.MAIN_PAGE}
                        />
                    </NotificationScreen>
                )
            }
        </Grid>
    )
}

export default BuildsPage
