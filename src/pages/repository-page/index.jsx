import React, { useCallback, useEffect } from 'react'
import {
    Headline1,
    TableHead,
    TableCellHead,
    TableRow,
    TableCell,
    ButtonPrimary,
    PageLoader, Link
} from '@sbol/design-system'
import { useTranslation } from 'react-i18next'
import { useSelector, useDispatch } from 'react-redux'
import { useHistory, useParams } from 'react-router-dom'
import moment from 'moment'
import { ic24ChevronLeft } from '@sbol/design-system/core/icon/common'

import QualityGatesModal from '../../components/quality-gates-modal'
import { Grid, NotificationScreen, StatusButton } from '../../components/UI'
import { setQualityGatesModalBranch } from '../../__data__/slices/repositories-slice'
import { repositoriesSelectors, userSelectors } from '../../__data__/selectors'

import { useRepository } from './use-repository'
import { TableStyled } from './repositories-page.style'

export const RepositoriesPage = () => {
    const dispatch = useDispatch()
    const history = useHistory()

    const { t } = useTranslation()
    const { repoId } = useParams()
    const { getRepoInfo, getActiveBuildsByTeamId } = useRepository(repoId)
    const repoInfo = useSelector(repositoriesSelectors.currentRepoSelector)
    const { name = '', branchList = [] } = repoInfo
    const team = useSelector(userSelectors.teamSelector)

    useEffect(() => {
        getRepoInfo(repoId)
        if (team.id) {
            getActiveBuildsByTeamId(team.id)
        }
    }, [team, repoId])
   
    const { qgModalBranchInfo } = useSelector((state) => state.repositories)
    
    const openModal = (branch) => {
        dispatch(setQualityGatesModalBranch(branch))
    }

    const closeModal = () => {
        dispatch(setQualityGatesModalBranch(null))
    }

    const handlePreviousPage = useCallback(() => {
        history.goBack()
    }, [])

    if (!name) {
        return <PageLoader />
    }
    
    return (
        <Grid fullWidth direction="column">
            <Link
                title={t('link.back.title')}
                underlined={false}
                mode="breadcrumb"
                icon={ic24ChevronLeft}
                onClick={handlePreviousPage}
                verticalMargin="open"
                verticalMarginDirection="bottom"
            />
            <Headline1 indent="zero">
                { name }
            </Headline1>
            <Grid fullWidth direction="column" size="lg" verticalMargin="open" verticalMarginDirection="top">
                {
                    Boolean(branchList.length) ? (
                        <TableStyled>
                            <TableHead>
                                <TableCellHead>Название ветки</TableCellHead>
                                <TableCellHead>Версия модуля</TableCellHead>
                                <TableCellHead>Последняя сборка</TableCellHead>
                                <TableCellHead>Дата последней сборки</TableCellHead>
                                <TableCellHead>QG</TableCellHead>
                                <TableCellHead>Коммит</TableCellHead>
                                <TableCellHead>Действия</TableCellHead>
                            </TableHead>
                            { branchList?.map((branch) => (<TableRow key={branch.id}>
                                <TableCell>{ branch.id }</TableCell>
                                <TableCell align="middle">{ branch.moduleVersion }</TableCell>
                                <TableCell>
                                    {
                                        branch.lastBuild?.id  ||  (
                                            <StatusButton
                                                status={branch.lastBuild?.id}
                                                horizontalMargin="zero"
                                                horizontalMarginDirection="both"
                                            />
                                        )
                                    }
                                </TableCell>
                                <TableCell>{
                                    branch.lastBuild
                                        ? moment(branch.lastBuild.createTimestamp).format('YY.MM.DD HH:mm')
                                        : (
                                            <StatusButton
                                                horizontalMargin="zero"
                                                horizontalMarginDirection="both"
                                            />
                                        )
                                }</TableCell>
                                <TableCell>
                                    <StatusButton
                                        status={branch.lastBuild?.state}
                                        horizontalMargin="nano"
                                        horizontalMarginDirection="both"
                                    />
                                </TableCell>
                                <TableCell>
                                    {
                                        branch.lastBuild?.commit || (
                                            <StatusButton
                                                horizontalMargin="zero"
                                                horizontalMarginDirection="both"
                                            />
                                        )
                                    }

                                </TableCell>
                                <TableCell><ButtonPrimary title="Собрать" onClick={() => openModal(branch)} /></TableCell>
                            </TableRow>
                            )) }
                        </TableStyled>
                    ) : (
                        <NotificationScreen title="тут веток нет" />
                    )
                }
            </Grid>
            { qgModalBranchInfo ? <QualityGatesModal
                {...{ repoInfo, closeModal, getRepoInfo }}
                branchInfo={qgModalBranchInfo}
            /> : null }
        </Grid>
    )
}
export default RepositoriesPage
