import { useDispatch, useSelector } from 'react-redux'
import { useCallback } from 'react'

import { actions } from '../../__data__'

export const useRepository = (repoId) => {
    const dispatch = useDispatch()
    const getRepoInfo = useCallback((repoId) => {return dispatch(actions.reposActions.getRepository(repoId))}, [])
    const getActiveBuildsByTeamId = useCallback((teamId) => dispatch(actions.executorActions.getActiveBuildsByTeamId(teamId)), [])

    return {
        getRepoInfo,
        getActiveBuildsByTeamId
    }
}
