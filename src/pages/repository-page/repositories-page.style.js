import styled from '@emotion/styled'
import { Table } from '@sbol/design-system'

export const TableStyled = styled(Table)`
  width: 100%;
`
