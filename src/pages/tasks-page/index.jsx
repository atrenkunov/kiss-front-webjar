import React, { useCallback, useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import {
    Headline1,
    Link,
    PageLoader,
    TableCell,
    TableHead,
} from '@sbol/design-system'
import { useTranslation } from 'react-i18next'

import { Grid } from '../../components/UI/grid'
import { useUserInfo } from '../../utils/use-user-info'
import { NotificationScreen } from '../../components/UI/notification-screen'
import { TableStyled } from '../stands-page/stands-page.style'
import { APP_ROUTES, TASK_NEW_BRANCH_LINK } from '../../__data__/constants'

import { TaskRow } from './task-row'
import { useTaskPage } from './use-task-page'

const TABLE_HEADERS = [
    {
        key: 'task',
        title: 'Задача'
    },
    {
        key: 'branchId',
        title: 'Номер ветки'
    },
    {
        key: 'version',
        title: 'Версия модуля'
    },
    {
        key: 'lastBuildNumber',
        title: 'Номер последней сборки'
    },
    {
        key: 'lastBuildStatus',
        title: 'Статус сборки'
    }
]

export const TasksPage = () => {
    const [loading, setLoading] = useState(true)
    const { t } = useTranslation()
    const history = useHistory()

    const { team } = useUserInfo()
    const { tasks = [], fetchTeamTasks, buildStages = [], fetchBuildStages } = useTaskPage()

    const handleTasksFetch = useCallback(async () => {
        setLoading(true)
        await fetchTeamTasks(team.id)
        setLoading(false)
    }, [team.id])

    const handleRedirect = useCallback((id) => {
        history.push(TASK_NEW_BRANCH_LINK(id))
    }, [])

    useEffect(() => {
        if (team.id) {
            handleTasksFetch()
        }
    }, [team.id])

    useEffect(() => {
        if (!buildStages.length) {
            fetchBuildStages()
        }
    }, [])

    return (
        <Grid
            direction="column"
        >
            <Headline1
                fontWeight="semibold"
                indent="zero"
            >
                {t('tasks.page.header')}
            </Headline1>
            <Grid
                fullWidth
                direction="column"
                size="lg"
                verticalMargin="open"
                verticalMarginDirection="top"
            >
                {loading ? <PageLoader /> : (
                    <Grid fullWidth direction="column">
                        {Boolean(tasks.length) ? (
                            <TableStyled size="md">
                                <TableHead>
                                    {TABLE_HEADERS.map((header) => (
                                        <TableCell key={header.key}>{header.title}</TableCell>
                                    ))}
                                </TableHead>
                                {tasks.map((task) => <TaskRow key={task.taskId} task={task} onClick={handleRedirect} />)}
                            </TableStyled>
                        ) : (
                            <NotificationScreen
                                title={t('notifications.empty.title.tasks')}
                                description={t('notifications.empty.description.tasks')}
                            >
                                <Link
                                    fontWeight="semibold"
                                    underlined={false}
                                    colorScheme="success"
                                    title="Перейти"
                                    href={APP_ROUTES.PLANNER}
                                />
                            </NotificationScreen>
                        )}
                    </Grid>
                )}
            </Grid>
        </Grid>
    )
}

export default TasksPage
