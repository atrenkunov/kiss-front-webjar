import { useDispatch, useSelector } from 'react-redux'
import { useCallback } from 'react'

import { actions, selectors } from '../../__data__'

export const useTaskPage = () => {
    const dispatch = useDispatch()

    const tasks = useSelector(selectors.tasksSelectors.tasksListSelector)
    const buildStages = useSelector(selectors.executorSelectors.buildStagesSelector)
    const fetchTeamTasks = useCallback((teamId) => dispatch(actions.tasksActions.getTeamTasks(teamId)), [dispatch])
    const fetchBuildStages = useCallback(() => dispatch(actions.executorActions.getBuildStages()), [dispatch])

    return {
        tasks,
        buildStages,

        fetchTeamTasks,
        fetchBuildStages
    }
}
