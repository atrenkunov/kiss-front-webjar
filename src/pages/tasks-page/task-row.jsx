import React, { useCallback } from 'react'
import { useTranslation } from 'react-i18next'
import {
    Body2,
    ButtonTertiary,
    ButtonTransparent,
    ExternalLink,
    Link,
    TableCell,
    TableRow,
    TooltipClick,
    Tip
} from '@sbol/design-system'

import { Grid } from '../../components/UI/grid'
import { HTML_ENTITIES, REPOSITORY_LINK } from '../../__data__/constants'
import {beautify, getStagesTip} from '../../utils'
import { StatusButton } from '../../components/UI/status-button'

import { useTaskPage } from './use-task-page'
import {useHistory} from "react-router-dom";


export const TaskRow = ({ task = {}, onClick }) => {
    const { t } = useTranslation()
    const history = useHistory()
    const { branch = {} } = task
    const { buildStages = [] } = useTaskPage()

    const handleClick = useCallback(() => {
        onClick(task.taskId)
    }, [task.taskId])

    const TaskTitleWrapper = task.jiraUrl ? ExternalLink : Body2

    const handleRedirect = useCallback(() => {
        history.push(REPOSITORY_LINK(branch?.repositoryId))
    }, [branch?.repositoryId])

    // fixme: веток к задаче может быть несколько
    // fixme: не приходит статус задачи и урл
    // fixme: урл на билд и версию модуля в нексусе?
    return (
        <>
            <TableRow key={task.taskId}>
                <TableCell>
                    <Grid direction="column">
                        <TaskTitleWrapper
                            title={task.taskId}
                            fontWeight="semibold"
                            verticalMargin="zero"
                            href={task.jiraUrl}
                        >
                            {task.taskId}
                        </TaskTitleWrapper>
                        <Body2 verticalMargin="zero" colorScheme="secondary">
                            {task?.taskName}
                        </Body2>
                    </Grid>
                </TableCell>
                <TableCell>
                    <Grid direction="column">
                        <Link
                            onClick={handleRedirect}
                            title={branch?.repositoryId}
                            fontWeight="semibold"
                            verticalMargin="open"
                            verticalMarginDirection="bottim"
                        />
                        {branch?.id || (
                            <ButtonTertiary
                                verticalMargin="zero"
                                horizontalMargin="zero"
                                title={t('tasks.page.create.branch')}
                                onClick={handleClick}
                            />
                        )}
                    </Grid>
                </TableCell>
                <TableCell>{branch?.moduleVersion || beautify(HTML_ENTITIES.EM_DASH)}</TableCell>
                <TableCell>
                    <Grid direction="column">
                        {branch?.lastBuild?.id  || beautify(HTML_ENTITIES.EM_DASH)}
                    </Grid>
                </TableCell>
                <TableCell>
                    {
                        branch?.lastBuild?.state ? (
                            <Grid justify="flex-start" align="center">
                                <StatusButton
                                    status={branch?.lastBuild?.state}
                                    verticalPadding="micro"
                                    horizontalPadding="micro"
                                    verticalMargin="zero"
                                    horizontalMargin="nano"
                                    horizontalMarginDirection="right"
                                />
                                <TooltipClick>
                                    <ButtonTransparent
                                        verticalPadding="nano"
                                        horizontalPadding="nano"
                                        verticalMargin="zero"
                                        horizontalMargin="zero"
                                        iconName="icon:sberboard/common/ic24Info"
                                    />
                                    <Tip
                                        title={t('stages.tip.build')}
                                        description={getStagesTip(branch.lastBuild.stages, buildStages)}
                                    />
                                </TooltipClick>

                            </Grid>
                        ) : beautify(HTML_ENTITIES.EM_DASH)
                    }
                </TableCell>
            </TableRow>
        </>
    )
}
