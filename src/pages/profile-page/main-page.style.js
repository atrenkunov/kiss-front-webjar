import styled from "@emotion/styled";
import {css} from '@emotion/react'

export const BranchContainer = styled.div(({theme}) => css`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    padding: 24px 0 36px;
    
    &:not(:last-child) {
      border-bottom: 1px solid ${theme.elevationOneBorderNormal}
    }
`)

export const TitleContainer = styled.div`
display: flex;
flex-direction: row;
justify-content: flex-start;
align-items: center;
`

export const BranchIconContainer = styled.div`
  width: 30px;
  height: 30px;
`
