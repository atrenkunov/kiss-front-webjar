import React from 'react'
import { useTranslation } from 'react-i18next'
import { Headline1 } from '@sbol/design-system'

import { Grid } from '../../components/UI'


export const ProfilePage = () => {
    const { t }  = useTranslation()
    return (
        <>
            <Grid direction="column">
                <Headline1 indent="zero">
                    {t('profile.page.header')}
                </Headline1>
            </Grid>
        </>
    )
}

export default ProfilePage
