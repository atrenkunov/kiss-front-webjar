import React, { useCallback, useEffect, useRef } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { Headline1, Link } from '@sbol/design-system'
import { ic24ChevronLeft } from '@sbol/design-system/core/icon/common'
import { useTranslation } from 'react-i18next'

import { Grid } from '../../components/UI/grid'
import { JIRA_API } from '../../__data__/api-map'

export const ViewerPage = () => {
    const { viewId } = useParams()
    const history = useHistory()
    const frameRef = useRef(null)

    const { t } = useTranslation()

    useEffect(() => {
        if (frameRef.current) {
            handleFrameRequest()
        }
    }, [frameRef.current])

    // fixme: Меня заставили
    const handleFrameRequest = useCallback(async () => {
        const response = await JIRA_API.requestPage(viewId)
        frameRef.current.contentWindow.document.write(response.data)
    }, [viewId, frameRef.current])
    
    const handlePreviousPage = useCallback(() => {
        history.goBack()
    }, [])

    return (
        <Grid direction="column">
            <Link
                title={t('link.back.title')}
                underlined={false}
                mode="breadcrumb"
                icon={ic24ChevronLeft}
                onClick={handlePreviousPage}
                verticalMargin="open"
                verticalMarginDirection="bottom"
            />
            <Headline1
                fontWeight="semibold"
                verticalMargin="nano"
            >
                {t('viewer.page.header', { viewId })}
            </Headline1>
            <iframe
                style={{ width: '100%', height: '100vh' }}
                ref={frameRef}
            />
        </Grid>
    )
}

export default ViewerPage
