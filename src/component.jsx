import React, { useState, lazy, Suspense } from 'react'
import { Route, Switch, BrowserRouter } from 'react-router-dom'
import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import { Provider } from 'react-redux'
import { PageLoader } from '@sbol/design-system'

import { ThemeWrapper } from './components/UI'
import { NavBar } from './components/nav-bar'
import './reset.css'
import localesRu from './locales/ru.json'
import { ThemeContext } from './__data__/contexts/theme-context'
import { APP_ROUTES, LIGHT_THEME } from './__data__/constants'
import { store } from './__data__'
import { ContentWrapper, GlobalStyle } from './components/UI/global-styles'
import { config } from './constants/config'

i18n
    .use(initReactI18next)
    .init({
        resources: {
            ru: {
                translation: localesRu
            }
        },
        fallbackLng: 'ru',

    })

const MainPage = lazy(() => import(/* webpackChunkName: "main-page"*/ './pages/profile-page'))

const TasksPage = lazy(() => import(/* webpackChunkName: "tasks-page"*/ './pages/tasks-page'))
const NewTaskPage = lazy(() => import(/* webpackChunkName: "new-task-page"*/ './pages/new-task-page'))
const NewBranchPage = lazy(() => import(/* webpackChunkName: "new-branch-page"*/ './pages/new-branch-page'))

const RepositoriesPage = lazy(() => import(/* webpackChunkName: "repositories-page"*/ './pages/repositories-page'))
const RepositoryPage = lazy(() => import(/* webpackChunkName: "repository-page"*/ './pages/repository-page'))
const NewRepositoryPage = lazy(() => import(/* webpackChunkName: "new-repository-page"*/ './pages/new-repository-page'))

const BuildsPage = lazy(() => import(/* webpackChunkName: "builds-page"*/ './pages/builds-page'))
const BuildPage = lazy(() => import(/* webpackChunkName: "build-page"*/ './pages/build-page'))

const StandsPage = lazy(() => import(/* webpackChunkName: "stands-page"*/ './pages/stands-page'))
const NewStandPage = lazy(() => import(/* webpackChunkName: "new-stand-page"*/ './pages/new-stand-page'))

const IntegrationsPage = lazy(() => import(/* webpackChunkName: "integrations-page"*/ './pages/integrations-page'))
const PlannerPage = lazy(() => import(/* webpackChunkName: "planner-page"*/ './pages/planner-page'))
const ViewerPage = lazy(() => import(/* webpackChunkName: "viewer-page"*/ './pages/viewer-page'))

const App = () => {
    const [theme, setTheme] = useState(LIGHT_THEME)
    const [isSideMenuOpened, setIsSideMenuOpened] = useState(!config.isExtension)
    
    return (
        <Provider store={store}>
            <ThemeContext.Provider value={{ theme, setTheme }}>
                <ThemeWrapper>
                    <GlobalStyle />
                    <BrowserRouter>
                        <NavBar isSideMenuOpened={isSideMenuOpened} setIsSideMenuOpened={setIsSideMenuOpened}/>
                        <ContentWrapper isSideMenuOpened={isSideMenuOpened}>
                            <Suspense fallback={<PageLoader />}>
                                <Switch>
                                    <Route exact path={APP_ROUTES.MAIN_PAGE} component={RepositoriesPage} />
                                    <Route exact path={APP_ROUTES.PROFILE} component={MainPage} />
                                    <Route exact path={APP_ROUTES.REPOSITORY} component={RepositoryPage} />
                                    <Route exact path={APP_ROUTES.CREATE_NEW_REPOSITORY} component={NewRepositoryPage} />
                                    <Route exact path={APP_ROUTES.TASKS} component={TasksPage} />
                                    <Route exact path={APP_ROUTES.TASK_NEW_BRANCH} component={NewBranchPage} />
                                    <Route exact path={APP_ROUTES.TASKS_NEW} component={NewTaskPage} />
                                    <Route exact path={APP_ROUTES.BUILDS} component={BuildsPage} />
                                    <Route exact path={APP_ROUTES.BUILD} component={BuildPage} />
                                    <Route exact path={APP_ROUTES.STANDS} component={StandsPage} />
                                    <Route exact path={APP_ROUTES.CREATE_NEW_STAND} component={NewStandPage} />
                                    <Route exact path={APP_ROUTES.INTEGRATIONS} component={IntegrationsPage} />
                                    <Route exact path={APP_ROUTES.PLANNER} component={PlannerPage} />
                                    <Route exact path={APP_ROUTES.VIEWER} component={ViewerPage} />
                                </Switch>
                            </Suspense>
                        </ContentWrapper>
                    </BrowserRouter>
                </ThemeWrapper>
            </ThemeContext.Provider>
        </Provider>
    )
}

export default App
