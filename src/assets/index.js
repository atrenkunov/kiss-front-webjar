import {
    success,
    waiting,
    warning,
    info,
    ic36Tag,
    ic24Cross,
    ic24ArrowRightSquare,
    ic24ChevronUp,
    ic24ChevronDown,
    ic36DocumentOnDocument,
    ic36Clock,
    ic36CircleCross,
    ic36Checkmark,
    ic24Plus,
    ic36CounterClockwise,
    ic36Info,
    ic24Info,
    ic36Exclamation,
    ic24Bolt,
    ic24CheckmarkCircle,
    ic24UserOnUser
} from '@sbol/design-system/core/icon/common/index.js'

import Burger from './burger.svg'
import Add from './add.svg'

export {
    Add,
    Burger,

    ic36Clock,
    ic24Bolt,
    ic36CircleCross,
    ic36Checkmark,
    success,
    waiting,
    warning,
    info,
    ic36Tag,
    ic24Cross,
    ic24Plus,
    ic24ArrowRightSquare,
    ic24ChevronDown,
    ic24ChevronUp,
    ic36DocumentOnDocument,
    ic36CounterClockwise,
    ic36Info,
    ic24Info,
    ic36Exclamation,
    ic24CheckmarkCircle,
    ic24UserOnUser
}
