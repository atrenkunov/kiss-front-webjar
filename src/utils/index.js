import Typograf from 'typograf'
import i18n from 'i18next'

import {API_RESPONSE_STATUSES, STAGE_DESCRIPTIONS} from '../__data__/constants'

const tp = new Typograf({ locale: ['ru', 'en-US'] })

export const beautify = (string) => tp.execute(string)

const ERROR_LOCALES = {
    BRANCH_ALREADY_EXISTS: 'error.branch.duplicate'
}
export const handleAPIError = (error) => {
    const { status, code } = error.response?.data || {}

    if (status >= API_RESPONSE_STATUSES.SERVER) {
        return {
            code,
            title: i18n.t(`${ERROR_LOCALES[code] || 'error.default'}.title`),
            text: i18n.t(`${ERROR_LOCALES[code] || 'error.default'}.text`),
        }
    }
    
    return {
        title: i18n.t('error.default.title'),
        text: i18n.t('error.default.text'),
    }
}

export const getInitials = (names) => {
    let letters = ''
    names.forEach((name) => letters += name.substring(0, 1).toUpperCase())
    return letters
}

export const getStagesTip = (stages = [], dictionary = []) => {
    let text = ''

    stages.forEach((stage) => {
        const name = dictionary.find((word) => word.id === stage.id)?.name || ''
        text += `__${name}__: ${STAGE_DESCRIPTIONS[stage.state]}\n\n`
    })

    return text

}
export const isPending = (action) => action.type.endsWith('/pending')
export const isFulfilled = (action) => action.type.endsWith('/fulfilled')
export const isRejected = (action) => action.type.endsWith('/rejected')
