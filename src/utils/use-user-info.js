import { useDispatch, useSelector } from 'react-redux'
import { useCallback } from 'react'

import { actions, selectors } from '../__data__'

export const useUserInfo = () => {
    const dispatch = useDispatch()

    const info = useSelector(selectors.userSelectors.infoSelector)
    const team = useSelector(selectors.userSelectors.teamSelector)
    const fetchInfo = useCallback(() => dispatch(actions.userActions.getUserInfo()), [dispatch])

    return {
        info,
        team,

        fetchInfo
    }
}
