import React from 'react'
import ReactDOM from 'react-dom'
import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import { IconLoader } from '@sbol/design-system'

import './reset.css'
import localesRu from './locales/ru.json'
import Component from './component'

IconLoader.addIcons('icon:sberboard/common', () => import('./assets/index'))

i18n
    .use(initReactI18next)
    .init({
        resources: {
            ru: {
                translation: localesRu
            }
        },
        fallbackLng: 'ru',

    })

if (module.hot) {
    module.hot.accept()
}

ReactDOM.render(
    <Component />,
    document.getElementById('root')
)
